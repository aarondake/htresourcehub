<?php 
    session_start();

    include('connection.php');
   
    $sql = "SELECT distinct p.pgm_id, pgm_agcy, pgm_name, pgm_stmnt, cont_phone, cont_web, cont_email, usa_city, state_name, hrs_open, hrs_close, hrs_day, hrs_247, hrs_desc, cont_hotline from programs p left join offered_resources o_r on p.pgm_id = o_r.pgm_id inner join resources r on o_r.rsrc_id = r.rsrc_id left join contact_details cd on p.pgm_id = cd.pgm_id left join program_locations pl on p.pgm_id = pl.pgm_id inner join usa_locations ul on pl.usa_id = ul.usa_id inner join usa_states us on ul.state_id = us.state_id left join program_hours ph on p.pgm_id = ph.pgm_id";
    $programs = explode(",", $_GET["fields"]);
    if (strlen($programs[0]) > 0) {
        
        $where  = " WHERE ";
        foreach ($programs as $key => $value) {
            // echo $value . "<br />";
            switch ($value) {
                case "Housing-Shelter":
                    $where .= "r.rsrc_id = 1 AND ";
                    break;
                case "Housing-Transitional":
                    $where .= "r.rsrc_id = 2 AND ";
                    break;
                case "Housing-Locate":
                    $where .= "r.rsrc_id = 3 AND ";
                    break;
                case "Clothing":
                    $where .= "r.rsrc_id = 4 AND ";
                    break;
                case "Food":
                    $where .= "r.rsrc_id = 5 AND ";
                    break;
                case "Employment":
                    $where .= "r.rsrc_id = 6 AND ";
                    break;
                case "Mentoring":
                    $where .= "r.rsrc_id = 7 AND ";
                    break;
                case "Counseling":
                    $where .= "r.rsrc_id = 8 AND ";
                    break;
                case "Pregnancy":
                    $where .= "r.rsrc_id = 9 AND ";
                    break;
                case "Medical":
                    $where .= "r.rsrc_id = 10 AND ";
                    break;
                case "Legal":
                    $where .= "r.rsrc_id = 11 AND ";
                    break;
                case "Government":
                    $where .= "r.rsrc_id = 12 AND ";
                    break;
                case "Investgation":
                    $where .= "r.rsrc_id = 13 AND ";
                    break;
                case "Foster-Care":
                    $where .= "r.rsrc_id = 14 AND ";
                    break;
                case "Awareness-Education":
                    $where .= "r.rsrc_id = 15 AND ";
                    break;
                case "Response-Training":
                    $where .= "r.rsrc_id = 16 AND ";
                    break;
                /*case "":
                    $where .= "r.rsrc_id =  AND ";
                    break;*/
            }
        // takes the last " AND " off.
        $where = substr($where,0, -5);
        // adds the where clause to the select statement
        $sql .= $where;
        }
    }
      
    $result = $conn->query($sql);

if ($result->num_rows > 0) {
        // output data of each row
            while($row = $result->fetch_assoc()) {
            $str = '';    
            $str .= '<h3>'.$row["pgm_agcy"].'</h3>';
            $str .= '<h4>'.$row["pgm_name"].'</h4>';
            $str .= '<h4>'.$row["usa_city"] .', '. $row["state_name"]. '</h4>';
            if ($row["hrs_open"] != "") {
                $str .= '<h4>'.$row["hrs_open"] .' - ' . $row["hrs_close"] . '(' . $row["hrs_day"] .')</h4>';
            }
            if ($row["hrs_247"] != "") {
                $str .= '<h4>24-Hour Hotline (' . $row["cont_hotline"] . ')</h4>';    
            }
            $str .= '<span style="float:right; display:inline-block;" onClick="load_details('.$row["pgm_id"].')">View Details</span><br/>';
            $str .= '<hr />';
            echo $str;
        }
    } else {
        echo "0 results";
    }
    $conn->close();


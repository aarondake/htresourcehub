var resource = false;
var age = false;
var gender = false;
var national = false;
var blocation = false;
var isLocked = true;
var role = "guest";
var filter="";
var recordArray=[];
var selections=[];

function setFilters() {
    filter = '?fields=';
	crumbs="";
	for (i = 0; i < selections.length; i++) { 
        if(selections[i]!== undefined) {
			    filter = filter.concat(selections[i],',');
				crumbs = crumbs+"<span style='color:#666666;'> | </span>"+selections[i];
		}
	}
	// removes the last comma ","
    filter = filter.substr(0,filter.length - 1);

	if(selections.length==0) {
		breadcrumbs.innerHTML = "<h4>Welcome to Human Trafficking Resource Hub</h4>";
	}else{
		if(crumbs=="") {
			breadcrumbs.innerHTML = "<h4>Welcome to Human Trafficking Resource Hub</h4>";
		}else{
			breadcrumbs.innerHTML = "<span style='color:#666666;'> Selected: </span>"+crumbs;
		}
		loadXMLDoc("search.php".concat(filter), showResults);
	}
}

function setResults(HTML) {
    document.getElementById("results").innerHTML=HTML;
}

/*function loadXMLDoc()
{
    var xmlhttp;
    if (window.XMLHttpRequest)
      {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
      }
    else
      {// code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            setResults(xmlhttp.responseText);
        }
      };

    // for (var i=1; i<filter.length; i++ ) {   }
    xmlhttp.open("GET","search.php".concat(filter),true);
    xmlhttp.send();
}*/

var xmlhttp;
function loadXMLDoc(url,cfunc) {
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }else{
        // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=cfunc;
    xmlhttp.open("GET",url,true);
    xmlhttp.send();
}

function showResults() { 
    if (xmlhttp.readyState===4 && xmlhttp.status===200) {
        document.getElementById("results").innerHTML=xmlhttp.responseText;
    }
}

function showDetails(id) {
    alert(id);
    loadXMLDoc("searchDetails.php?id="+id,function(){
        if (xmlhttp.readyState===4 && xmlhttp.status===200) {
            document.getElementById("light").innerHTML=xmlhttp.responseText;
        }
    });
}

function resetForm() {
document.getElementById("WARchest").reset();
}
function addRecord() {
	alert("addRecord function in main.js")
}

function checkUser() {
	if(calc(document.getElementById('loginform').thePass.value)=="fbc5bc5fd5ee1fb52fb055b6e5bca673"){
		TweenLite.to(login, 0.5, {css:{visibility:"hidden"}});
		TweenLite.to(logout, 0.5, {css:{visibility:"visible"}});
		TweenLite.to(menuRecords, 0.5, {css:{visibility:"visible"}});
		TweenLite.to(menuLogin, 1, {css:{top:"-65px"},ease:Quad.easeOut});
		role="war";
		isLocked=false;
		document.getElementById('loginform').thePass.value = "";
	}else if(calc(document.getElementById('loginform').thePass.value)=="4acb4bc224acbbe3c2bfdcaa39a4324e"){
		TweenLite.to(login, 0.5, {css:{visibility:"hidden"}});
		TweenLite.to(logout, 0.5, {css:{visibility:"visible"}});
		TweenLite.to(menuLogin, 1, {css:{top:"-65px"},ease:Quad.easeOut});
		role="admin";
		isLocked=false;
		document.getElementById('loginform').thePass.value = "";
	}else if(calc(document.getElementById('loginform').thePass.value)=="504c314447e32ded4096fb0b470ace4f"){
		TweenLite.to(login, 0.5, {css:{visibility:"hidden"}});
		TweenLite.to(logout, 0.5, {css:{visibility:"visible"}});
		TweenLite.to(menuLogin, 1, {css:{top:"-65px"},ease:Quad.easeOut});
		role="editor";
		isLocked=false;
		document.getElementById('loginform').thePass.value = "";
	}else{
		alert("sorry password does not match");
		TweenLite.to(menuLogin, 1, {css:{top:"-65px"},ease:Quad.easeOut});
		TweenLite.to(openit, 0.5, {css:{visibility:"hidden"}});
		TweenLite.to(menuRecords, 0.5, {css:{visibility:"hidden"}});
		TweenLite.to(menuResource, 0.5, {css:{visibility:"hidden"}});
		TweenLite.to(newRecord, 0.5, {css:{visibility:"hidden"}});
		document.getElementById('loginform').thePass.value = "";
		role="guest";
	}	
}
function enterInfo() {
	menuReset();
	TweenLite.to(results, 0.5, {css:{visibility:"hidden"}});
	TweenLite.to(newRecord, 0.5, {css:{visibility:"visible"}});
}
function theUser() {
	if(isLocked===true) {
		TweenLite.to(menuLogin, 1, {css:{top:"65px"},ease:Quad.easeOut});	
	} else {
		TweenLite.to(results, 0.5, {css:{visibility:"visible"}});
		TweenLite.to(logout, 0.5, {css:{visibility:"hidden"}});
		TweenLite.to(login, 0.5, {css:{visibility:"visible"}});
		TweenLite.to(newRecord, 0.5, {css:{visibility:"hidden"}});
		TweenLite.to(menuRecords, 0.5, {css:{visibility:"hidden"}});
		TweenLite.to(menuLogin, 1, {css:{top:"-65px"},ease:Quad.easeOut});
		isLocked=true;
	}
}
function aboutWrite() {
	var message = "<h1>Stuff Passed from Somewhere</h1><p>address here<br/>more address<br/>more stuff that may or may not be important</p>";
	light.innerHTML = message;	
}
window.document.onkeydown = function (e)
{
    if (!e){
        e = event;
    }
    if (e.keyCode === 27){
        lightbox_close();
    }
};
function lightbox_open(){
    window.scrollTo(0,0);
    document.getElementById('light').style.display='block';
    document.getElementById('fade').style.display='block';  
}
function lightbox_close(){
    document.getElementById('light').style.display='none';
    document.getElementById('fade').style.display='none';
}
function checkShelter() {
	if(selections[0]==undefined || selections[0]=="") {
   		 TweenLite.to(check1, 2, {css:{background:"#ae0001"}});
        selections[0]="Housing-Shelter";
	}else{
		TweenLite.to(check1, 2, {css:{background:"#ffffff"}});
		selections[0]=undefined;
	}
	setFilters();
}
function checkTransit() {
	if(selections[1]==undefined || selections[1]=="") {
   		 TweenLite.to(check2, 2, {css:{background:"#ae0001"}});
        selections[1]="Housing-Transitional";
	}else{
		TweenLite.to(check2, 2, {css:{background:"#ffffff"}});
		selections[1]=undefined;
	}
	setFilters();
}
function checkLocate() {
	if(selections[2]==undefined || selections[2]=="") {
   		 TweenLite.to(check3, 2, {css:{background:"#ae0001"}});
        selections[2]="Housing-Locate";
	}else{
		TweenLite.to(check3, 2, {css:{background:"#ffffff"}});
		selections[2]=undefined;
	}
	setFilters();
}
function checkClothing() {	
	if(selections[3]==undefined || selections[3]=="") {
   		 TweenLite.to(check4, 2, {css:{background:"#ae0001"}});
        selections[3]="Clothing";
	}else{
		TweenLite.to(check4, 2, {css:{background:"#ffffff"}});
		selections[3]=undefined;
	}
	setFilters();
}
function checkFood() {	
	if(selections[4]==undefined || selections[4]=="") {
   		 TweenLite.to(check5, 2, {css:{background:"#ae0001"}});
        selections[4]="Food";
	}else{
		TweenLite.to(check5, 2, {css:{background:"#ffffff"}});
		selections[4]=undefined;
	}
	setFilters();
}
function checkEmployment() {	
	if(selections[5]==undefined || selections[5]=="") {
   		 TweenLite.to(check6, 2, {css:{background:"#ae0001"}});
        selections[5]="Employment";
	}else{
		TweenLite.to(check6, 2, {css:{background:"#ffffff"}});
		selections[5]=undefined;
	}
	setFilters();
}
function checkMentoring() {	
	if(selections[6]==undefined || selections[6]=="") {
   		 TweenLite.to(check7, 2, {css:{background:"#ae0001"}});
        selections[6]="Mentoring";
	}else{
		TweenLite.to(check7, 2, {css:{background:"#ffffff"}});
		selections[6]=undefined;
	}
	setFilters();
}
function checkCounseling() {	
	if(selections[7]==undefined || selections[7]=="") {
   		 TweenLite.to(check8, 2, {css:{background:"#ae0001"}});
        selections[7]="Counseling";
	}else{
		TweenLite.to(check8, 2, {css:{background:"#ffffff"}});
		selections[7]=undefined;
	}
	setFilters();
}
function checkPregnancy() {	
	if(selections[8]==undefined || selections[8]=="") {
   		 TweenLite.to(check9, 2, {css:{background:"#ae0001"}});
        selections[8]="Pregnancy";
	}else{
		TweenLite.to(check9, 2, {css:{background:"#ffffff"}});
		selections[8]=undefined;
	}
	setFilters();
}
function checkMedical() {	
	if(selections[9]==undefined || selections[9]=="") {
   		 TweenLite.to(check10, 2, {css:{background:"#ae0001"}});
        selections[9]="Medical";
	}else{
		TweenLite.to(check10, 2, {css:{background:"#ffffff"}});
		selections[9]=undefined;
	}
	setFilters();
}
function checkLegal() {	
	if(selections[10]==undefined || selections[10]=="") {
   		 TweenLite.to(check11, 2, {css:{background:"#ae0001"}});
        selections[10]="Legal";
	}else{
		TweenLite.to(check11, 2, {css:{background:"#ffffff"}});
		selections[10]=undefined;
	}
	setFilters();
}
function checkGovernmental() {	
	if(selections[11]==undefined || selections[11]=="") {
   		 TweenLite.to(check12, 2, {css:{background:"#ae0001"}});
        selections[11]="Government";
	}else{
		TweenLite.to(check12, 2, {css:{background:"#ffffff"}});
		selections[11]=undefined;
	}
	setFilters();
}
function checkInvest() {	
	if(selections[12]==undefined || selections[12]=="") {
   		 TweenLite.to(check13, 2, {css:{background:"#ae0001"}});
        selections[12]="Investgation";
	}else{
		TweenLite.to(check13, 2, {css:{background:"#ffffff"}});
		selections[12]=undefined;
	}
	setFilters();
}
function checkFoster() {	
	if(selections[13]==undefined || selections[13]=="") {
   		 TweenLite.to(check14, 2, {css:{background:"#ae0001"}});
        selections[13]="Foster-Care";
	}else{
		TweenLite.to(check14, 2, {css:{background:"#ffffff"}});
		selections[13]=undefined;
	}
	setFilters();
}
function checkAware() {	
	if(selections[14]==undefined || selections[14]=="") {
   		 TweenLite.to(check15, 2, {css:{background:"#ae0001"}});
        selections[14]="Awareness-Education";
	}else{
		TweenLite.to(check15, 2, {css:{background:"#ffffff"}});
		selections[14]=undefined;
	}
	setFilters();
}
function checkResponse() {	
	if(selections[15]==undefined || selections[15]=="") {
   		 TweenLite.to(check16, 2, {css:{background:"#ae0001"}});
        selections[15]="Response-Training";
	}else{
		TweenLite.to(check16, 2, {css:{background:"#ffffff"}});
		selections[15]=undefined;
	}
	setFilters();
}
function checkToddler() {	
	if(selections[16]==undefined || selections[16]=="") {
   		 TweenLite.to(check17, 2, {css:{background:"#ae0001"}});
        selections[16]="Infant-Toddler";
	}else{
		TweenLite.to(check17, 2, {css:{background:"#ffffff"}});
		selections[16]=undefined;
	}
       setFilters();	
}
function checkChildren() {	
	if(selections[17]==undefined || selections[17]=="") {
   		 TweenLite.to(check18, 2, {css:{background:"#ae0001"}});
        selections[17]="Children";
	}else{
		TweenLite.to(check18, 2, {css:{background:"#ffffff"}});
		selections[17]=undefined;
	}
       setFilters();	
}
function checkYouth() {	
	if(selections[18]==undefined || selections[18]=="") {
   		 TweenLite.to(check19, 2, {css:{background:"#ae0001"}});
        selections[18]="Youth";
	}else{
		TweenLite.to(check19, 2, {css:{background:"#ffffff"}});
		selections[18]=undefined;
	}
      setFilters();	
}
function checkAdult() {	
	if(selections[19]==undefined || selections[19]=="") {
   		 TweenLite.to(check20, 2, {css:{background:"#ae0001"}});
        selections[19]="Adult";
	}else{
		TweenLite.to(check20, 2, {css:{background:"#ffffff"}});
		selections[19]=undefined;
	}
      setFilters();	
}
function checkMale() {	
	if(selections[20]==undefined || selections[20]=="") {
   		 TweenLite.to(check21, 2, {css:{background:"#ae0001"}});
        selections[20]="Male";
	}else{
		TweenLite.to(check21, 2, {css:{background:"#ffffff"}});
		selections[20]=undefined;
	}
       setFilters();	
}
function checkFemale() {	
	if(selections[21]==undefined || selections[21]=="") {
   		 TweenLite.to(check22, 2, {css:{background:"#ae0001"}});
        selections[21]="Female";
	}else{
		TweenLite.to(check22, 2, {css:{background:"#ffffff"}});
		selections[21]=undefined;
	}
       setFilters();	
}
function checkTransMale() {	
	if(selections[22]==undefined || selections[22]=="") {
   		 TweenLite.to(check23, 2, {css:{background:"#ae0001"}});
        selections[22]="Transgender-Male";
	}else{
		TweenLite.to(check23, 2, {css:{background:"#ffffff"}});
		selections[22]=undefined;
	}
       setFilters();	
}
function checkTransFemale() {	
	if(selections[23]==undefined || selections[23]=="") {
   		 TweenLite.to(check24, 2, {css:{background:"#ae0001"}});
        selections[23]="Transgender-Female";
	}else{
		TweenLite.to(check24, 2, {css:{background:"#ffffff"}});
		selections[23]=undefined;
	}
      setFilters();	
}
function checkDomestic() {	
	if(selections[24]==undefined || selections[24]=="") {
   		 TweenLite.to(check25, 2, {css:{background:"#ae0001"}});
        selections[24]="Domestic-Born";
	}else{
		TweenLite.to(check25, 2, {css:{background:"#ffffff"}});
		selections[24]=undefined;
	}
       setFilters();	
}
function checkForeign() {	
	if(selections[25]==undefined || selections[25]=="") {
   		 TweenLite.to(check26, 2, {css:{background:"#ae0001"}});
        selections[25]="Foreign-Born";
	}else{
		TweenLite.to(check26, 2, {css:{background:"#ffffff"}});
		selections[25]=undefined;
	}
       setFilters();	
}
function checkUndoc() {	
	if(selections[26]==undefined || selections[26]=="") {
   		 TweenLite.to(check27, 2, {css:{background:"#ae0001"}});
        selections[26]="Undocumented";
	}else{
		TweenLite.to(check27, 2, {css:{background:"#ffffff"}});
		selections[26]=undefined;
	}
       setFilters();	
}
function checkState() {	
	  selections[27]=document.getElementById('states').selectState.value;
	  if(document.getElementById('states').selectState.value=="Michigan") {
		 TweenLite.to(counties, 0, {css:{visibility:"visible"}});
	  } else {
		  TweenLite.to(counties, 0, {css:{visibility:"hidden"}});
	  }
      setFilters();	
}
function checkCounty() {
	  selections[28]=document.getElementById('counties').County.value;
      setFilters();	
}
function menuReset() {
	TweenLite.to(menuResource, 0.5, {css:{left:"300px"}});
	TweenLite.to(menuResource, 0.5, {css:{opacity:0}});
	TweenLite.to(plusResource, 0.5, {css:{opacity:100}});
	resource=false;
	TweenLite.to(menuAge, 0.5, {css:{left:"300px"}});
	TweenLite.to(menuAge, 0.5, {css:{opacity:0}});
	TweenLite.to(plusAge, 0.5, {css:{opacity:100}});
	age=false;
	TweenLite.to(menuGender, 0.5, {css:{left:"300px"}});
	TweenLite.to(menuGender, 0.5, {css:{opacity:0}});
	TweenLite.to(plusGender, 0.5, {css:{opacity:100}});
	gender=false;
	TweenLite.to(menuNational, 0.5, {css:{left:"300px"}});
	TweenLite.to(menuNational, 0.5, {css:{opacity:0}});
	TweenLite.to(plusNational, 0.5, {css:{opacity:100}});
	national=false;
	TweenLite.to(menuLocation, 0.5, {css:{left:"300px"}});
	TweenLite.to(menuLocation, 0.5, {css:{opacity:0}});
	TweenLite.to(plusLocation, 0.5, {css:{opacity:100}});
	blocation=false;
}
function goResource() {
	if(resource==false) {
		menuReset();
		TweenLite.to(menuResource, 0.5, {css:{opacity:100}});
		TweenLite.to(menuResource, 1, {css:{left:"15px"},ease:Quad.easeOut});
		TweenLite.to(plusResource, 0.5, {css:{opacity:0}});
		TweenLite.to(results, 0.5, {css:{visibility:"visible"}});
		TweenLite.to(newRecord, 0.5, {css:{visibility:"hidden"}});
	    resource=true;
	}else{
		TweenLite.to(menuResource, 0.5, {css:{left:"300px"}});
		TweenLite.to(menuResource, 0.2, {css:{opacity:0}});
		TweenLite.to(plusResource, 0.5, {css:{opacity:100}});
		TweenLite.to(results, 0.5, {css:{visibility:"visible"}});
		TweenLite.to(newRecord, 0.5, {css:{visibility:"hidden"}});
		resource=false;
	}
}
//buttonResource.goResource();
function goAge() {
	if(age==false) {
		menuReset();
		TweenLite.to(menuAge, 1, {css:{left:"15px"},ease:Quad.easeOut});
		TweenLite.to(menuAge, 0.2, {css:{opacity:100}});
		TweenLite.to(plusAge, 0.5, {css:{opacity:0}});
		TweenLite.to(results, 0.5, {css:{visibility:"visible"}});
		TweenLite.to(newRecord, 0.5, {css:{visibility:"hidden"}});
		age=true;
	}else{
		TweenLite.to(menuAge, 0.5, {css:{left:"300px"}});
		TweenLite.to(menuAge, 0.2, {css:{opacity:0}});
		TweenLite.to(plusAge, 0.5, {css:{opacity:100}});
		TweenLite.to(results, 0.5, {css:{visibility:"visible"}});
		TweenLite.to(newRecord, 0.5, {css:{visibility:"hidden"}});
		age=false;
	}
}
//buttonAge.goAge();
function goGender() {
	if(gender==false) {
		menuReset();
		TweenLite.to(menuGender, 1, {css:{left:"15px"},ease:Quad.easeOut});
		TweenLite.to(menuGender, 0.2, {css:{opacity:100}});
		TweenLite.to(plusGender, 0.5, {css:{opacity:0}});
		TweenLite.to(results, 0.5, {css:{visibility:"visible"}});
		TweenLite.to(newRecord, 0.5, {css:{visibility:"hidden"}});
		gender=true;
	}else{
		TweenLite.to(menuGender, 0.5, {css:{left:"300px"}});
		TweenLite.to(menuGender, 0.2, {css:{opacity:0}});
		TweenLite.to(plusGender, 0.5, {css:{opacity:100}});
		TweenLite.to(results, 0.5, {css:{visibility:"visible"}});
		TweenLite.to(newRecord, 0.5, {css:{visibility:"hidden"}});
		gender=false;
	}
}
//buttonGender.goGender();
function goNational() {
	if(national==false) {
		menuReset();
		TweenLite.to(menuNational, 1, {css:{left:"15px"},ease:Quad.easeOut});
		TweenLite.to(menuNational, 0.2, {css:{opacity:100}});
		TweenLite.to(plusNational, 0.5, {css:{opacity:0}});
		TweenLite.to(results, 0.5, {css:{visibility:"visible"}});
		TweenLite.to(newRecord, 0.5, {css:{visibility:"hidden"}});
		national=true;
	}else{
		TweenLite.to(menuNational, 0.5, {css:{left:"300px"}});
		TweenLite.to(menuNational, 0.2, {css:{opacity:0}});
		TweenLite.to(plusNational, 0.5, {css:{opacity:100}});
		TweenLite.to(results, 0.5, {css:{visibility:"visible"}});
		TweenLite.to(newRecord, 0.5, {css:{visibility:"hidden"}});
		national=false;
	}
}
function goLocation() {
	if(blocation==false) {
		menuReset();
		TweenLite.to(menuLocation, 1, {css:{left:"15px"},ease:Quad.easeOut});
		TweenLite.to(menuLocation, 0.2, {css:{opacity:100}});
		TweenLite.to(plusLocation, 0.5, {css:{opacity:0}});
		TweenLite.to(results, 0.5, {css:{visibility:"visible"}});
		TweenLite.to(newRecord, 0.5, {css:{visibility:"hidden"}});
		blocation=true;
	}else{
		TweenLite.to(menuLocation, 0.5, {css:{left:"300px"}});
		TweenLite.to(plusLocation, 0.5, {css:{opacity:100}});
		TweenLite.to(menuLocation, 0.2, {css:{opacity:0}});
		TweenLite.to(results, 0.5, {css:{visibility:"visible"}});
		TweenLite.to(newRecord, 0.5, {css:{visibility:"hidden"}});
		blocation=false;
	}
}
//buttonNational.goNational();


<?php
session_start();
include('connection.php');
    
$str = "";
$login = filter_input(INPUT_POST, 'confidential');
$pgm_name = filter_input(INPUT_POST, 'programName');
$pgm_agcy = filter_input(INPUT_POST, 'umbrella');
$pgm_stmnt = filter_input(INPUT_POST, 'statement');
$cont_phone = filter_input(INPUT_POST, 'phone');
$cont_hotline = filter_input(INPUT_POST, 'hotline');
$cont_web = filter_input(INPUT_POST, 'website');
$cont_email = filter_input(INPUT_POST, 'email');
$conPhone = filter_input(INPUT_POST, 'conPhone');
$prime_confide = filter_input(INPUT_POST, 'contactConfidential');
$prime_name = filter_input(INPUT_POST, 'priName');
$prime_position = filter_input(INPUT_POST, 'position');
$prime_phone = filter_input(INPUT_POST, 'priPhone');
$prime_email = filter_input(INPUT_POST, 'priEmail');

$street = filter_input(INPUT_POST, 'street');
$city = filter_input(INPUT_POST, 'city');
$county = filter_input(INPUT_POST, 'county');
$state = filter_input(INPUT_POST, 'state');
$zip = filter_input(INPUT_POST, 'zip');
$street2 = filter_input(INPUT_POST, 'street2');
$city2 = filter_input(INPUT_POST, 'city2');
$county2 = filter_input(INPUT_POST, 'county2');
$state2 = filter_input(INPUT_POST, 'state2');
$zip2 = filter_input(INPUT_POST, 'zip2');
$confide_street = filter_input(INPUT_POST, 'street3');
$confide_city = filter_input(INPUT_POST, 'city3');
$confide_county = filter_input(INPUT_POST, 'county3');
$confide_state = filter_input(INPUT_POST, 'state3');
$confide_zip = filter_input(INPUT_POST, 'zip3');
$hrs_247 = filter_input(INPUT_POST, '24-7');
$hrs_open = filter_input(INPUT_POST, 'hours-open');
$hrs_close = filter_input(INPUT_POST, 'hours-close');
$hrs_days = filter_input(INPUT_POST, 'days');
$addl_hrs_open = filter_input(INPUT_POST, 'add-hours-open');
$addl_hrs_close = filter_input(INPUT_POST, 'add-hours-close');
$addl_hrs_days = filter_input(INPUT_POST, 'add-days');
$addl_hrs_reason = filter_input(INPUT_POST, 'add-reason');

/*$ = filter_input(INPUT_POST, '');
$ = filter_input(INPUT_POST, '');
$ = filter_input(INPUT_POST, '');
$ = filter_input(INPUT_POST, '');*/

$procon_cost = filter_input(INPUT_POST, 'fee');
$procon_faith = filter_input(INPUT_POST, 'faith');
$procon_memb = filter_input(INPUT_POST, 'membership');
$procon_train = filter_input(INPUT_POST, 'training');
$procon_app = filter_input(INPUT_POST, 'application');
$procon_restrict = filter_input(INPUT_POST, 'waiting');

$male = filter_input(INPUT_POST, 'male');
$female = filter_input(INPUT_POST, 'female');
$trans_male = filter_input(INPUT_POST, 'transMale');
$trans_female = filter_input(INPUT_POST, 'transFemale');
$infant = filter_input(INPUT_POST, 'infant');
$child = filter_input(INPUT_POST, 'children');
$youth = filter_input(INPUT_POST, 'youth');
$adult = filter_input(INPUT_POST, 'adults');
$age_ranges = filter_input(INPUT_POST, 'ageRanges');
$domestic_born = filter_input(INPUT_POST, 'domestic');
$foreign_born = filter_input(INPUT_POST, 'foreign');
$undocumented = filter_input(INPUT_POST, 'undocumented');

$race = filter_input(INPUT_POST, 'race');
$ethnicity = filter_input(INPUT_POST, 'ethnicity');

$procon_notes = filter_input(INPUT_POST, 'notes');
$confide_notes = filter_input(INPUT_POST, 'conNotes');
$war = filter_input(INPUT_POST, 'warReview');

date_default_timezone_set('America/New_York');
$date = date('Y/m/d H:i:s');

$error_str = "";
try {
    /* disable autocommit */
    $conn->autocommit(FALSE);
    
    // First of all, let's begin a transaction
    $conn->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);

    // Insert into Programs
    $sql = "INSERT INTO programs (pgm_name, pgm_agcy ,pgm_stmnt, login, war, pgm_add, pgm_lastcheck, confidential_phone)
            VALUES('$pgm_name','$pgm_agcy','$pgm_stmnt', $login, $war, '$date', '$date', '$conPhone')";
    $conn->query($sql);
    
    // get the primary key
    $pgm_id = $conn->insert_id;
    
    // Insert into table: contact_details
    $sql = "INSERT INTO contact_details (`pgm_id`, `cont_phone`, `cont_hotline`, `cont_web`, `cont_email`)
            VALUES($pgm_id, '$cont_phone', '$cont_hotline', '$cont_web', '$cont_email')";
    $conn->query($sql);
    
    // Insert into table: primary_contacts
    $sql = "INSERT INTO primary_contacts (`pgm_id`,`prime_name`,`prime_position`,`prime_phone`,`prime_email`, `confidential`)
            VALUES($pgm_id, '$prime_name', '$prime_position', '$prime_phone', '$prime_email', $prime_confide)";
    $conn->query($sql);
        
    // insert into Program Addresses
    $sql = "INSERT INTO program_addresses (`pgm_id`, `street`, `city`, `county`, `state`, `zip`, `street2`, `city2`, `county2`, `state2`, `zip2`, `confide_street`, `confide_city`, `confide_county`, `confide_state`, `confide_zip`) 
            VALUES ($pgm_id, '$street', '$city', '$county', '$state', '$zip', '$street2', '$city2', '$county2', '$state2', '$zip2', '$confide_street', '$confide_city', '$confide_county', '$confide_state', '$confide_zip')";
    $conn->query($sql);
    
    
    $resources = array(
        1 => "shelter",
        2 => "trans",
        3 => "locate",
        4 => "clothing",
        5 => "food",
        6 => "employment",
        7 => "mentoring",
        8 => "counseling",
        9 => "pregnancy",
        10 => "medical",
        11 => "legal",
        12 => "government",
        13 => "invest",
        14 => "foster",
        15 => "awareness",
        16 => "response",
        17 => "other"
        );
    
    // insert into table: offered_resources
    $sql = "";
    foreach ($resources as $key => $value) {
        $service = filter_input(INPUT_POST, $value.'Service');
        $supply = filter_input(INPUT_POST, $value.'Supply');
        $emergency = filter_input(INPUT_POST, $value.'Emg');
        $description = filter_input(INPUT_POST, $value.'Desc');
        if ($service == 1 ||
            $supply == 1 ||
            $emergency == 1 ||
            strlen($description) > 0)
        {
            // At least one of the checkboxes is checked, or there is a description...add to database!
            $sql = "INSERT INTO offered_resources (pgm_id, rsrc_id, rsrcoff_service, rsrcoff_supply, rsrcoff_emgcy, rsrcoff_desc)
                   VALUES ($pgm_id, $key, $service, $supply, $emergency, '$description')";
            $conn->query($sql);
        }
    }

    // Insert into: Program Considerations
    $sql = "INSERT INTO program_considerations (pgm_id, procon_cost, procon_faith, procon_memb, procon_train, procon_app, procon_restrict, procon_notes)
            VALUES($pgm_id, '$procon_cost', '$procon_faith', '$procon_memb', '$procon_train', '$procon_app', '$procon_restrict', '$procon_notes')";
    $conn->query($sql);
    
    // Insert into: Demographics
    $sql = "INSERT INTO demographics (pgm_id, male, female, trans_male, trans_female, infant, child, youth, adult, domestic_born, foreign_born, undocumented, race, ethnicity, age_ranges)
            VALUES ($pgm_id, '$male', '$female', '$trans_male', '$trans_female', '$infant', '$child', '$youth', '$adult', '$domestic_born', '$foreign_born', '$undocumented', '$race', '$ethnicity', '$age_ranges')";
    $conn->query($sql);
    
    // Insert into: Program Hours
    $sql = "INSERT INTO program_hours (pgm_id, hrs_open, hrs_close, hrs_days, hrs_247, addl_hrs_open, addl_hrs_close, addl_hrs_days, addl_hrs_reason) 
            VALUES ($pgm_id, '$hrs_open', '$hrs_close', '$hrs_days', $hrs_247, '$addl_hrs_open', '$addl_hrs_close', '$addl_hrs_days', '$addl_hrs_reason');";
    $conn->query($sql);

    //// If we arrive here, it means that no exception was thrown
    // i.e. no query has failed, and we can commit the transaction
    $conn->commit();
    
} catch (Exception $e) {
    // An exception has been thrown
    // We must rollback the transaction
    $conn->rollback();
    // Exit with a FAILED message will tell the calling AJAX function
    // to display an error message, and not clear the form fields.
    $error_str="ADD RECORD FAILED! \n\n" . $e.message;
}
$conn->close();
mysqli_report(MYSQLI_REPORT_OFF);
// if any error occurred, this will be set, if not then return empy string, meaning all is a-o-kay!
echo $error_str;

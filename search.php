<?php 
    session_start();
    if (!isset($offset)) {
        $offset = 0;
    }
    // set up connection (included in another file so that it can be ignored by git, since dev and production are different).
    include('connection.php');
    
    // select statement for querying what programs are available based on the resources selected.
    $sql = "SELECT p.pgm_id, pgm_name, count(r.rsrc_name) 'rsrc_count'
            from programs p
            inner join offered_resources o_r on p.pgm_id = o_r.pgm_id 
            inner join resources r on o_r.rsrc_id = r.rsrc_id 
            left join demographics d on p.pgm_id = d.pgm_id \r\n";

    // Add WHERE clause (filter by resources needed)
    if (strlen($_POST["resources"]) > 0) {
        $resources = explode(",", $_POST["resources"]);
        $resource_count = sizeof($resources);
        $where  = "WHERE ";
        foreach ($resources as $key => $value) {
            switch ($value) {
                case "Housing-Shelter":
                    $where .= "r.rsrc_id = 1 OR ";
                    break;
                case "Housing-Transitional":
                    $where .= "r.rsrc_id = 2 OR ";
                    break;
                case "Housing-Locate":
                    $where .= "r.rsrc_id = 3 OR ";
                    break;
                case "Clothing":
                    $where .= "r.rsrc_id = 4 OR ";
                    break;
                case "Food":
                    $where .= "r.rsrc_id = 5 OR ";
                    break;
                case "Employment":
                    $where .= "r.rsrc_id = 6 OR ";
                    break;
                case "Mentoring":
                    $where .= "r.rsrc_id = 7 OR ";
                    break;
                case "Counseling":
                    $where .= "r.rsrc_id = 8 OR ";
                    break;
                case "Pregnancy":
                    $where .= "r.rsrc_id = 9 OR ";
                    break;
                case "Medical":
                    $where .= "r.rsrc_id = 10 OR ";
                    break;
                case "Legal":
                    $where .= "r.rsrc_id = 11 OR ";
                    break;
                case "Government":
                    $where .= "r.rsrc_id = 12 OR ";
                    break;
                case "Investgation":
                    $where .= "r.rsrc_id = 13 OR ";
                    break;
                case "Foster-Care":
                    $where .= "r.rsrc_id = 14 OR ";
                    break;
                case "Awareness-Education":
                    $where .= "r.rsrc_id = 15 OR ";
                    break;
                case "Response-Training":
                    $where .= "r.rsrc_id = 16 OR ";
                    break;
            }
        }
        
        // takes the last " OR " off.
        $where = substr($where,0, -4);
    }
    
    if (strlen($_POST["demographics"]) > 0) {
        $demographics = explode(",", $_POST["demographics"]);
        //$demographics_count = sizeof($demographics);  // don't need this right now
        
        // if there is already a where clause for the programs, then add an
        // AND to the end.  Else, start the WHERE clause off fresh.
        if (strlen($where) > 0 ) {$where  .= " AND ";}else{$where = " WHERE ";}
        
        foreach ($demographics as $key => $value) {
            switch ($value) {
                case "Infant-Toddler":
                    $where .= "d.infant = 1 AND ";
                    break;
                case "Children":
                    $where .= "d.child = 1 AND ";
                    break;
                case "Youth":
                    $where .= "d.youth = 1 AND ";
                    break;
                case "Adult":
                    $where .= "d.adult = 1 AND ";
                    break;
                case "Male":
                    $where .= "d.male = 1 AND ";
                    break;
                case "Female":
                    $where .= "d.female = 1 AND ";
                    break;
                case "Transgender-Male":
                    $where .= "d.trans_male = 1 AND ";
                    break;
                case "Transgender-Female":
                    $where .= "d.trans_female = 1 AND ";
                    break;
                case "Domestic-Born":
                    $where .= "d.domestic_born = 1 AND ";
                    break;
                case "Foreign-Born":
                    $where .= "d.foreign_born = 1 AND ";
                    break;
                case "Undocumented":
                    $where .= "d.undocumented = 1 AND ";
                    break;
            }
        }
        // takes the last " AND " off.
        $where = substr($where,0, -5);
    }

    // adds the where clause to the select statement
    if (strlen($where) > 0) {
        $sql .= $where ."\r\n";
    }
    
    
    // Add GROUP BY clause
    $sql .= "GROUP BY p.pgm_id \r\n";
            
    
    // Add HAVING clause, if necessary. This makes sure that the number of offered resources in the query matches the number that was selected by the end user.
    if ($resource_count > 0) {
        $sql .= " HAVING count(r.rsrc_id) = $resource_count \r\n";
    }
    $sql .= "ORDER BY pgm_name;";
    try
    {
        $result = $conn->query($sql);
    }catch(Exception $e) {
        throw $e;
    }

    if ($result->num_rows == 0) {
        echo "<p>No results have been found. For more resource options, please expand your search. For additional resources, please visit <a href='http://www.hwmuw.org/211onlinedatabase' target='_blank'>2-1-1 Online Database</a></p>";
    } else {
            
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $sql2 =  "SELECT pgm_name, pgm_agcy, pgm_stmnt, cont_phone, cont_web, cont_email, "
                . "hrs_open, hrs_close, hrs_days, hrs_247, addl_hrs_open, "
                . "addl_hrs_close, addl_hrs_days, addl_hrs_reason, cont_hotline "
                . "from programs p "
                . "left join contact_details cd on p.pgm_id = cd.pgm_id "
                . "left join program_hours ph on p.pgm_id = ph.pgm_id "
                . "WHERE p.pgm_id = ". $row['pgm_id'].";";
            try
            {
                $result2 = $conn->query($sql2);
            }catch(Exception $e) {
                throw $e;
            }
        while($row2 = $result2->fetch_assoc()) {
            $str = '<div class="resource-list"><p>OFFERED RESOURCES<p><ul>';    
            $sql3 = "SELECT r.rsrc_name from resources r inner join offered_resources o_r on r.rsrc_id = o_r.rsrc_id where o_r.pgm_id = " . $row['pgm_id'] . ";";
            try {
                $result3 = $conn->query($sql3);
            }catch(Exception $e) {
                throw $e;
            }
            while ($row3 = $result3->fetch_assoc()) {
                $str .= "<li>". $row3["rsrc_name"] . "</li>";
            }
            $str .= '</ul></div>';

            $str .= '<h1 onClick="showDetails('.$row["pgm_id"].')" style="cursor: pointer;">'.$row2["pgm_name"].'</h1>';
            if ($row2["pgm_name"] != $row2["pgm_agcy"]){
                // name does not equal agency, so display that too
                $str .= '<h2>'.$row2["pgm_agcy"].'</h2>';
            }
            if (strlen($row["city"])>0) {
                $str .= '<h4>'.$row2["city"]; 
                if (strlen($row["state"])>0) {
                    $str .= ', '. $row2["state"];
                }
                $str .= '</h4>';
            }
            if ($row2["hrs_open"] != "") {
                $str .= '<p>'.$row2["hrs_open"] .' - ' . $row2["hrs_close"] . ' (' . $row2["hrs_days"] .')</p>';
            } 
			if ($row2["cont_phone"] != "") {
				$str .= '<p>Phone: ' . $row2["cont_phone"] . '</p>';
			}
            if ($row2["hrs_247"] != "") {
                $str .= '<h5>24-Hour Hotline';
				if ($row2["cont_hotline"] != "")
				{
					$str .= " (" . $row2["cont_hotline"] . ')';
				}
				$str .=	'</h5>';    
            }
			if ($row2["cont_web"] != "") {
				$str .= '<p>Website: <a href="http://' . $row2["cont_web"] . ' "target="_blank">' . $row2["cont_web"] . '</a></p>';
			}
            if ($row2["cont_email"] != "") {
				$str .= '<p>Email: ' . $row2["cont_email"] . '</p>';
			}

            //$str .= '<p style="float:left;">' . $row2["hrs_desc"] . '</p>';
            //$str .= '<span style="float:right; display:inline-block;" onClick="showDetails('.$row2["pgm_id"].')">View Details</span><br/>';
            $str .= '<br/><hr /><br/>';
            echo $str;
        }
    }
}
$conn->close();
mysqli_report(MYSQLI_REPORT_OFF);
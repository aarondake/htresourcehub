<?php 
    session_start();

    include('connection.php');
   
    $sql = "SELECT pgm_agcy, pgm_name, pgm_stmnt, cont_phone, cont_web, cont_email, usa_city, state_name, hrs_open, hrs_close, hrs_day, hrs_247, hrs_desc, cont_hotline from programs p left join offered_resources o_r on p.pgm_id = o_r.pgm_id inner join resources r on o_r.rsrc_id = r.rsrc_id left join contact_details cd on p.pgm_id = cd.pgm_id left join program_locations pl on p.pgm_id = pl.pgm_id inner join usa_locations ul on pl.usa_id = ul.usa_id inner join usa_states us on ul.state_id = us.state_id left join program_hours ph on p.pgm_id = ph.pgm_id
            ";
    $pgmID = filter_input(INPUT_GET, 'id');
    $sql .= "WHERE p.pgm_id = $pgmID";
    $result = $conn->query($sql);

if ($result->num_rows > 0) {
        // output data of each row
            while($row = $result->fetch_assoc()) {
            $str = '';    
            if ($row["pgm_name"]===$row["pgm_agcy"]){
                //just display Program Name
                $str .= '<h1>'.$row["pgm_name"].'</h1>';
            }else {
                //display both program name and agency, since they are different
                $str .= '<h1>'.$row["pgm_name"].'</h1>';
                $str .= '<h2>'.$row["pgm_agcy"].'</h2>';
            }
            $str .= '<h4>'.$row["usa_city"] .', '. $row["state_name"]. '</h4>';
            if ($row["hrs_open"] != "") {
                $str .= '<h4>'.$row["hrs_open"] .' - ' . $row["hrs_close"] . '(' . $row["hrs_day"] .')</h4>';
            } 
            $str .= '<p>Phone: ' . $row["cont_phone"] . '</p>';
            $str .= '<p>Website: ' . $row["cont_web"] . '</p>';
            $str .= '<p>Email: ' . $row["cont_email"] . '</p>';
            if ($row["hrs_247"] != "") {
                $str .= '<h5>24-Hour Hotline (' . $row["cont_hotline"] . ')</h5>';    
            }
            $str .= '<span style="float:right; display:inline-block;" onClick="showDetails('.$row["pgm_id"].')">View Details</span><br/>';
            $str .= '<p>Email: ' . $row["hrs_desc"] . '</p>';
            $str .= '<hr />';
            echo $str;
        }
    } else {
        echo "<p>No results have been found. For more resource options, please expand your search. For additional resources, please visit <a href='http://www.hwmuw.org/211onlinedatabase' target='_blank'>2-1-1 Online Database</a></p>";
    }
    $conn->close();


<?php session_start() ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />

<meta http-equiv="x-ua-compatible" content="ie=edge" />

<title>Human Trafficking Resource Hub</title>
<script src="com/src/minified/TweenMax.min.js"></script>
<script src="com/src/md5.js"></script>
<script src="com/main.js"></script>
<?php include_once("analyticstracking.php"); ?>
<script language="JavaScript">
    function disableEnterKey(e)
    {	    
        var key; 
        if(window.event)
            key = window.event.keyCode;     //IE
        else
            key = e.which;     //firefox
        if(key == 13)
            return false;
        else
            return true;
    }
</script>


<!--<link rel="stylesheet" href="https://cdn.rawgit.com/twbs/bootstrap/v4-dev/dist/css/bootstrap.css">-->
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
</head>

<body>
<div class="container">
  <div class="row">
    <div class="Absolute-Center is-Responsive">
<div id="light">

</div>
<div id="fade" onClick="lightbox_close()"></div>
<div id="closeMe" onClick="menuReset()"></div>
<!--<script src="https://code.jquery.com/jquery.js"></script>-->
<!--<script src="com/src/bootstrap.min.js"></script>-->
<div id="login" onclick="theUser()"><img src="images/locked.png"/></div>
<div id="logout" onclick="theUser()"><img src="images/open.png"/></div>
<div id="header" onClick="menuReset()">
    <div id="logo"><a href="/"><img src="images/logo.png"/></a></div>
    <div id="circle"></div>
    <div id="grayBar"><div id="topText"><a href="/"><img src="images/header.png"/></a></div></div>
    <div id="redBar">
		<div class="menu" id="menuAbout" onClick="aboutWrite()">About</div>
		<div id="menuTraffic"><a href="http://stopthistraffic.org" class="menu" target="_blank">Stop This Traffic</a></div>
		<div id="menuApply"><a href="pdf/Human Trafficking Resource Hub Form.pdf" class="menu" target="_blank">Resource Application</a></div>
		<div class="menu" id="menuRecords" onClick="enterInfo()">Enter Records</div>
	</div>
</div>
<div id="breadcrumbs"><h4>Welcome to Human Trafficking Resource Hub</h4></div>
<div id="plusResource" onclick="goResource()"><img src="images/plus.png" width="24" height="24" /></div>
<div id="minusResource"><img src="images/minus.png" width="24" height="24" /></div>
<div id="buttonResource" onclick="goResource()"><p onmouseover="style.color='#ae0001'" onmouseout="style.color='#666666'">Resource</p></div>
<div id="plusAge" onclick="goAge()"><img src="images/plus.png" width="24" height="24" /></div>
<div id="minusAge"><img src="images/minus.png" width="24" height="24" /></div>
<div id="buttonAge" onclick="goAge()"><p onmouseover="style.color='#ae0001'" onmouseout="style.color='#666666'">Age</p></div>
<div id="plusGender" onclick="goGender()"><img src="images/plus.png" width="24" height="24" /></div>
<div id="minusGender"><img src="images/minus.png" width="24" height="24" /></div>
<div id="buttonGender" onclick="goGender()"><p onmouseover="style.color='#ae0001'" onmouseout="style.color='#666666'">Gender</p></div>
<div id="plusNational" onclick="goNational()"><img src="images/plus.png" width="24" height="24" /></div>
<div id="minusNational" onclick="goNational()"><img src="images/minus.png" width="24" height="24" /></div>
<div id="buttonNational" onclick="goNational()"><p onmouseover="style.color='#ae0001'" onmouseout="style.color='#666666'">Nationality</p></div>
<div id="plusLocation" onclick="goLocation()"><img src="images/plus.png" width="24" height="24" /></div>
<div id="minusLocation"><img src="images/minus.png" width="24" height="24" /></div>
<div id="buttonLocation" onclick="goLocation()"><p onmouseover="style.color='#ae0001'" onmouseout="style.color='#666666'">Location</p></div>
<div id="openit" onclick="lightbox_open()"><p>Open lightbox</p></div>
<div id="menuResource">
<div id="check15" class="checkBox" onclick="checkAware()"></div> <span onclick="checkAware()">Awareness/Education</span></br>
<div id="check4" class="checkBox" onclick="checkClothing()"></div> <span onclick="checkClothing()">Clothing</span></br>
<div id="check8" class="checkBox" onclick="checkCounseling()"></div> <span onclick="checkCounseling()">Counseling/Therapy</span></br>
<div id="check6" class="checkBox" onclick="checkEmployment()"></div> <span onclick="checkEmployment()">Employment</span></br>
<div id="check5" class="checkBox" onclick="checkFood()"></div> <span onclick="checkFood()">Food</span></br>
<div id="check14" class="checkBox" onclick="checkFoster()"></div> <span onclick="checkFoster()">Foster Care</span></br>
<div id="check12" class="checkBox" onclick="checkGovernmental()"></div> <span onclick="checkGovernmental()">Governmental</span></br>
<div id="check3" class="checkBox" onclick="checkLocate()"></div> <span onclick="checkLocate()">Housing - Locate</span> </br>
<div id="check1" class="checkBox" onclick="checkShelter()"></div> <span onclick="checkShelter()">Housing - Shelter</span></br>
<div id="check2" class="checkBox" onclick="checkTransit()"></div> <span onclick="checkTransit()">Housing - Transitional</span></br>
<div id="check13" class="checkBox" onclick="checkInvest()"></div> <span onclick="checkInvest()">Investigation</span></br>
<div id="check11" class="checkBox" onclick="checkLegal()"></div> <span onclick="checkLegal()">Legal</span></br>
<div id="check10" class="checkBox" onclick="checkMedical()"></div> <span onclick="checkMedical()">Medical</span></br>
<div id="check7" class="checkBox" onclick="checkMentoring()"></div> <span onclick="checkMentoring()">Mentoring</span></br>
<div id="check9" class="checkBox" onclick="checkPregnancy()"></div> <span onclick="checkPregnancy()">Pregnancy</span></br>
<div id="check16" class="checkBox" onclick="checkResponse()"></div> <span onclick="checkResponse()">Response Training</span></br>
</div>
<div id="menuAge">
<div id="check17" class="checkBox" onclick="checkToddler()"></div> <span onclick="checkToddler()">Infants/Toddlers</span></br>
<div id="check18" class="checkBox" onclick="checkChildren()"></div> <span onclick="checkChildren()">Children</span></br>
<div id="check19" class="checkBox" onclick="checkYouth()"></div> <span onclick="checkYouth()">Youth/Teen</span></br>
<div id="check20" class="checkBox" onclick="checkAdult()"></div> <span onclick="checkAdult()">Adult</span></br>
</div>
<div id="menuGender">
<div id="check22" class="checkBox" onclick="checkFemale()"></div> <span onclick="checkFemale()">Female</span></br>
<div id="check21" class="checkBox" onclick="checkMale()"></div> <span onclick="checkMale()">Male</span></br>
<div id="check24" class="checkBox" onclick="checkTransFemale()"></div> <span onclick="checkTransFemale()">Transgender Female</span></br>
<div id="check23" class="checkBox" onclick="checkTransMale()"></div> <span onclick="checkTransMale()">Transgender Male</span></br>
</div>
<div id="menuLogin">
    <div class="login">
        <form id="loginform">
            password:<br/><br/>
            <input name="thePass"  type="password" size="20" maxlength="40"  onKeyPress="return disableEnterKey(event)" /><br/><br/>
            <input class="buttonSubmit" name="submit" value="submit" onclick="checkUser()" type="button" />
        </form>
    </div>
</div>
<div id="menuNational">
<div id="check25" class="checkBox" onclick="checkDomestic()"></div> <span onclick="checkDomestic()">Domestic-Born</span></br>
<div id="check26" class="checkBox" onclick="checkForeign()"></div> <span onclick="checkForeign()">Foreign-Born</span></br>
<div id="check27" class="checkBox" onclick="checkUndoc()"></div> <span onclick="checkUndoc()">Undocumented</span></br>
</div>
<div id="menuLocation">
<form id="states">
    <select size="1"  id="selectState" onchange="checkState()">
    <?php include('option-states.txt'); ?>
    </select>
</form>
<br />
<form id="counties">
  		<select size="1" id="County" onclick="checkCounty()">
  		  <option value="Alcona" selected>Alcona</option>
  		  <option value="Alger">Alger</option>
  		  <option value="Allegan">Allegan</option>
  		  <option value="Alpena">Alpena</option>
  		  <option value="Antrim">Antrim</option>
  		  <option value="Arenac">Arenac</option>
  		  <option value="Baraga">Baraga</option>
  		  <option value="Barry">Barry</option>
  		  <option value="Bay">Bay</option>
  		  <option value="Benzie">Benzie</option>
  		  <option value="Berrien">Berrien</option>
  		  <option value="Branch">Branch</option>
  		  <option value="Brown">Brown</option>
  		  <option value="Calhoun">Calhoun</option>
  		  <option value="Cass">Cass</option>
  		  <option value="Charlevoix">Charlevoix</option>
  		  <option value="Cheboygan">Cheboygan</option>
  		  <option value="Chippewa">Chippewa</option>
  		  <option value="Clare">Clare</option>
  		  <option value="Clinton">Clinton</option>
  		  <option value="Crawford">Crawford</option>
  		  <option value="Delta">Delta</option>
  		  <option value="Dickinson">Dickinson</option>
  		  <option value="Eaton">Eaton</option>
  		  <option value="Emmet">Emmet</option>
  		  <option value="Genesee">Genesee</option>
  		  <option value="Gladwin">Gladwin</option>
  		  <option value="Gogebic">Gogebic</option>
  		  <option value="Grand Traverse">Grand Traverse</option>
  		  <option value="Gratiot">Gratiot</option>
  		  <option value="Hillsdale">Hillsdale</option>
  		  <option value="Houghton">Houghton</option>
  		  <option value="Huron">Huron</option>
  		  <option value="Ingham">Ingham</option>
  		  <option value="Ionia">Ionia</option>
  		  <option value="Iosco">Iosco</option>
  		  <option value="Iowa">Iowa</option>
  		  <option value="Iron">Iron</option>
  		  <option value="Isabella">Isabella</option>
  		  <option value="Isle Royal">Isle Royal</option>
  		  <option value="Jackson">Jackson</option>
  		  <option value="Kalamazoo">Kalamazoo</option>
  		  <option value="Kalkaska">Kalkaska</option>
  		  <option value="Kent">Kent</option>
  		  <option value="Keweenaw">Keweenaw</option>
  		  <option value="Lake">Lake</option>
  		  <option value="Lapeer">Lapeer</option>
  		  <option value="Leelanau">Leelanau</option>
  		  <option value="Lenawee">Lenawee</option>
  		  <option value="Livingston">Livingston</option>
  		  <option value="Luce">Luce</option>
  		  <option value="Mackinac">Mackinac</option>
  		  <option value="Macomb">Macomb</option>
  		  <option value="Manistee">Manistee</option>
  		  <option value="Manitou">Manitou</option>
  		  <option value="Marquette">Marquette</option>
  		  <option value="Mason">Mason</option>
  		  <option value="Mecosta">Mecosta</option>
  		  <option value="Menominee">Menominee</option>
  		  <option value="Michilimackinac">Michilimackinac</option>
  		  <option value="Midland">Midland</option>
  		  <option value="Missaukee">Missaukee</option>
  		  <option value="Monroe">Monroe</option>
  		  <option value="Montcalm">Montcalm</option>
  		  <option value="Montmorency">Montmorency</option>
  		  <option value="Muskegon">Muskegon</option>
  		  <option value="Newaygo">Newaygo</option>
  		  <option value="Oakland">Oakland</option>
  		  <option value="Oceana">Oceana</option>
  		  <option value="Ogemaw">Ogemaw</option>
  		  <option value="Ontonagon">Ontonagon</option>
  		  <option value="Osceola">Osceola</option>
  		  <option value="Oscoda">Oscoda</option>
  		  <option value="Otsego">Otsego</option>
  		  <option value="Ottawa">Ottawa</option>
  		  <option value="Presque Isle">Presque Isle</option>
  		  <option value="Roscommon">Roscommon</option>
  		  <option value="Saginaw">Saginaw</option>
  		  <option value="Sanilac">Sanilac</option>
  		  <option value="Schoolcraft">Schoolcraft</option>
  		  <option value="Shiawassee">Shiawassee</option>
  		  <option value="St. Clair">St. Clair</option>
  		  <option value="St. Joseph">St. Joseph</option>
  		  <option value="State Level Sites">State Level Sites</option>
  		  <option value="Tuscola">Tuscola</option>
  		  <option value="Van Buren">Van Buren</option>
  		  <option value="Washtenaw">Washtenaw</option>
  		  <option value="Wayne">Wayne</option>
  		  <option value="Wexford">Wexford</option>
    </select>
</form>
</div>
<div id="form-messages" style="position:absolute; left: 20px; top: 450px; width: 150px;"></div>
<div id="newRecord">
 <?php include 'admin-form.php'; ?>
</div>
<div id="results" onClick="menuReset()">
<p>Human trafficking is a form of modern-day slavery in which people are treated as property, as possessions to be used, controlled, and exploited for the gain of others. Lured by deceit or forced by coercion, victims of trafficking are exploited for commercial, labor, or sexual purposes. Though traffickers typically prey on the most vulnerable, this crime knows no socioeconomic boundsâ€”it takes place in communities rich and poor, urban and suburban, and is not a respecter of ethnicity, race, or economic status. Millions of people are trafficked in countries all over the worldâ€”in third-world countries and wealthy nations, including the United States, the &quot;land of the free.&quot;</p>
If you are a victim of trafficking or any other form of abuse or exploitation, this site is here to help you. If you know someone who is a victim, or if you suspect someone you know may be, this site functions as a resource for you as well. Whether you need food and shelter, counseling, medical care, legal advice, or other assistance; whether you suspect a case of trafficking and need to know what signs to look for; whether you want to know how you can help; or whether you are simply seeking information, this site exists to provide the resources and information you need.</p>
</div>

<div id="footer" onClick="menuReset()"><div id="footerInfo">PO Box 356 Grandville, MI  49468<br /><a href="mailto:info@warinternational.org" class="footerLink">info@warinternational.org</a><br />616 855-0796</div></div>
</div>
</div>
</div>
</body>
</html>
<script>
    search();
</script>

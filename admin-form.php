<style type="text/css">
    .blue {background-color: #A3CBFB;}
    .darkblue {background-color: #4FBAF7;}
    .gray {background-color: #F3F0F0;}
    .darkgray {background-color: #999999;}
    .pink {background-color: #FBEAEB;}
    .darkpink {background-color: #FBBDBE;}
    
    table#resourcesProvided tr:nth-child(even) {background: #FBEAEB}
    table#resourcesProvided tr:nth-child(odd) {background: #FBBDBE}
    
</style>
<script type="text/javascript">
           function checkness(ele, chk)
           {
               document.getElementById(chk).checked = (document.getElementById(ele).value.length > 0);   
           }
           
           // this is just a variable to make sure the event only runs once per value changed.
           var trigger = false;
           function feeChanged(element) {
               if (trigger) { return; }
               trigger = true;
               var chkFree = document.getElementById('chkFree');
               var chkFee = document.getElementById('chkFee');
               var fee = document.getElementById('fee');
               
               switch (element.id) {
                   case "chkFree":    
                       chkFee.checked = !chkFree.checked;
                       break;
                   case "chkFee":
                       chkFree.checked = !chkFee.checked;
                       break;        
                   case "fee":
                       if (fee.value === "" || fee.value.toLowerCase() === "free") {
                           chkFree.checked = true; 
                           chkFee.checked = false;
                       } else {
                           chkFree.checked = false;
                           chkFee.checked = true;
                       }
               }
               
               fee.disabled = chkFree.checked;
               if (chkFree.checked) {
                   fee.value = "Free";
               }
              
               
               trigger = false;
           }
           function togglePrimeContactConfidential() {
               isConfidential = document.getElementById('contactConfidential').checked;
               if (isConfidential){
                   document.getElementById('priContact').parentElement.className = "blue";
                   document.getElementById('priName').parentElement.className = "blue";
                   document.getElementById('position').parentElement.className = "blue";
                   document.getElementById('priPhone').parentElement.className = "blue";
                   document.getElementById('priEmail').parentElement.className = "blue";
               }else{
                   document.getElementById('priContact').parentElement.className = "";
                   document.getElementById('priName').parentElement.className = "";
                   document.getElementById('position').parentElement.className = "";
                   document.getElementById('priPhone').parentElement.className = "";
                   document.getElementById('priEmail').parentElement.className = ""; 
               }
               
           }
       </script>
       <form class="data" id="WARchest" method="post">
        <table width="720"  border="0" cellpadding="8" cellspacing="0">
          <tbody>
            <tr class="blue">
                <td colspan="2"><input id="confidential" name="confidential" type="checkbox" tabindex="0"> <label for="confidential">Entire Program is Confidential</label></td>
            </tr>
            <tr class="gray">
              <td>Program Name:<br /><input id="programName" name="programName" type="text" required tabindex="10" size="40" maxlength="60" ></td>
              <td>Umbrella Agency(if any):<br /><input id="umbrella" name="umbrella" type="text" tabindex="20" size="40" maxlength="60" ></td>
            </tr>
            <tr class="gray">
              <td colspan="2">Program Statement(Mission, Vision, Goals):<br/>
              <textarea id="statement" name="statement" cols="75" rows="5" maxlength="500" tabindex="30"></textarea></td>
            </tr>
            <tr class="pink">
              <td>Main Phone:<br /><input id="phone" name="phone" type="text" tabindex="40" size="40"></td>
              <td>Hotline:<br /><input id="hotline" name="hotline" type="text" tabindex="50" size="40"></td>
            </tr>
            <tr class="pink">
              <td>Website:<br /><input id="website" name="website" type="text" tabindex="60" size="40"></td>
              <td>Email:<br /><input id="email" name="email" type="text" tabindex="70" size="40"></td>
            </tr>
            <tr class="blue">
              <td colspan="2">Confidential Phone Number: <input id="conPhone" name="conPhone" type="tel" tabindex="80" size="40"></td>
            </tr>
            <tr class="gray">
                <td><label id='priContact'>Primary Contact</label></td>
                <td class="blue"><input id="contactConfidential" name="contactConfidential" type="checkbox" onchange="togglePrimeContactConfidential()" tabindex="90"><label for="contactConfidential"> Primary Contact is Confidential</label></td>
            </tr>
            <tr class="gray">
              <td>Name:<br /><input id="priName" name="priName" type="text" tabindex="100" size="40" maxlength="60" ></td>
              <td>Position:<br /><input id="position" name="position" type="text" tabindex="110" size="40" maxlength="60" ></td>
            </tr>
            <tr class="gray">
              <td>Phone:<br /><input id="priPhone" name="priPhone" type="text" tabindex="120" size="40"></td>
              <td>Email:<br /><input id="priEmail" name="priEmail" type="text" tabindex="130" size="40"></td>
            </tr>
            <tr class="darkpink">
              <td class="heads" colspan="2">Address 1</td>
            </tr>
            <tr class="pink">
              <td colspan="2" bgcolor="#F3F0F0">Street:<input id="street" name="street" type="text" tabindex="140" size="85" maxlength="90" ></td>
            </tr>
            <tr class="pink">
              <td>City:<br /><input id="city" name="city" type="text" tabindex="150" size="40"></td>
              <td>County:<br /><input id="county" name="county" tabindex="160" type="text" size="40"></td>
            </tr>
            <tr class="pink">
              <td>State:<br />
                  <select id="state" name="state" size="1" tabindex="170" style="width:255px">
                      <?php include('option-states.txt'); ?>
                </select>
              </td>
              <td>Zip Code:<br /><input id="zip"  name="zip" type="number" tabindex="180" size="10"></td>
            </tr>
            <tr class="darkgray">
              <td class="heads" colspan="2">Address 2</td>
            </tr>
            <tr class="gray">
              <td colspan="2">Street:<input id="street2" name="street2" type="text" tabindex="190" size="85" maxlength="90" ></td>
            </tr>
            <tr class="gray">
              <td>City:<br /><input id="city2" name="city2" type="text" tabindex="200" size="40"></td>
              <td>County:<br /><input id="county2" name="county2" type="text" tabindex="210" size="40"></td>
            </tr>
            <tr class="gray">
              <td>State:<br />
                <select id="state2" name="state2" size="1" tabindex="220" style="width:255px">
                      <?php include('option-states.txt'); ?>
                </select>
              </td>
              <td>Zip Code:<br /><input id="zip2" name="zip2" type="number" tabindex="230" size="10"></td>
            </tr>
            <tr class="darkblue">
              <td class="heads" colspan="2">Confidential Address</td>
            </tr>
            <tr class="blue">
              <td colspan="2">Street:<input id="street3" name="street3" type="text" tabindex="240" size="85" maxlength="90" ></td>
            </tr>
            <tr class="blue">
              <td>City:<br /><input id="city3" name="city3" type="text" tabindex="250" size="40"></td>
              <td>County:<br /><input id="county3"  name="county3" type="text"tabindex="260" size="40"></td>
            </tr>
            <tr class="blue">
              <td>State:<br />
                <select name="state3" size="1" tabindex="270" style="width:255px">
                      <?php include('option-states.txt'); ?>
                </select>
              </td>
              <td>Zip Code:<br /><input name="zip3" type="number" id="zip3" tabindex="280" size="10"></td>
            </tr>
            <tr class="darkgray">
              <td class="heads" colspan="2">Hours</td>
            </tr>
            <tr class="gray">
              <td><input name="24-7" tabindex="290" type="checkbox">24/7</td>
              <td>General Hours:<br />
                <select name="hours-open" type="text" id="hours-open" tabindex="300" size="1">
                    <?php include('option-times.txt'); ?>
                    <script>document.getElementById('hours-open').selectedIndex=17; /* 17 = 8am */</script>
                </select>
                  -
                <select name="hours-close" type="text" id="hours-close" tabindex="310" size="1">
                    <?php include('option-times.txt'); ?>
                    <script>document.getElementById('hours-close').selectedIndex=35; /* 35 = 5pm */</script>
                </select> <br/>
                <input type="text" name="days" size="40" tabindex="320" value="Monday - Friday" />
              </td>
            </tr>
            <tr class="gray">
              <td>Additional Hours:<br />
                <select id="add-hours-open" name="add-hours-open" type="text" tabindex="330" size="1">
                    <?php include('option-times.txt'); ?>
                </select>
                  -
                <select id="add-hours-close" name="add-hours-close" type="text" tabindex="340" size="1">
                    <?php include('option-times.txt'); ?>
                </select> <br/>
                <input type="text" name="add-days" size="40" tabindex="350" value="" />    
              </td>
              <td>Reason for Additional Hours:<br /><textarea name="reasonHours" type="text" id="reasonHours" tabindex="360" size="40" maxlength="200" style="resize:both;"></textarea></td>
            </tr>
          </tbody>
        </table>
        <table id="resourcesProvided" width="720"  border="0" cellpadding="8" cellspacing="0">
          <tbody>
            <tr class="heads">
              <td>Resource Provided</td>
              <td class="heads" bgcolor="#FBBDBE">Service</td>
              <td class="heads" bgcolor="#FBBDBE">Supply</td>
              <td class="heads" bgcolor="#FBBDBE">Emergency</td>
              <td class="heads" bgcolor="#FBBDBE">Description of Resource Provided</td>
            </tr>
            <tr>
              <td>Shelter</td>
              <td align="center"><input name="shelterService" tabindex="370" type="checkbox" disabled></td>
              <td align="center"><input name="shelterSupply" tabindex="380" type="checkbox"></td>
              <td align="center"><input name="shelterEmg" tabindex="390" type="checkbox"></td>
              <td><textarea id="shelterDesc" name="shelterDesc" tabindex="400" class="narrowTextarea"></textarea></td>
            </tr>
            <tr>
              <td>Transitional Housing</td>
              <td align="center"><input name="transService" type="checkbox" tabindex="410" disabled></td>
              <td align="center"><input name="transSupply" type="checkbox" tabindex="420"></td>
              <td align="center"><input name="transEmg" type="checkbox" tabindex="430"></td>
              <td><textarea id="transDesc" name="transDesc" tabindex="440" class="narrowTextarea"></textarea></td>
            </tr>
            <tr>
              <td>Locate Housing</td>
              <td align="center"><input name="locateService" type="checkbox" tabindex="450"></td>
              <td align="center"><input name="locateSupply" type="checkbox" disabled tabindex="460"></td>
              <td align="center"><input name="locateEmg" type="checkbox" tabindex="470"></td>
              <td><textarea id="locateDesc" name="locateDesc" tabindex="480" class="narrowTextarea"></textarea></td>
            </tr>
            <tr>
              <td>Clothing</td>
              <td align="center"><input name="clothingService" type="checkbox" tabindex="490"></td>
              <td align="center"><input name="clothingSupply" type="checkbox" tabindex="500"></td>
              <td align="center"><input name="clothingEmg" type="checkbox" tabindex="510"></td>
              <td><textarea id="clothingDesc" name="clothingDesc" tabindex="520" class="narrowTextarea"></textarea></td>
            </tr>
            <tr>
              <td>Food</td>
              <td align="center"><input name="foodService" type="checkbox" tabindex="530"></td>
              <td align="center"><input name="foodSupply" type="checkbox" tabindex="540"></td>
              <td align="center"><input name="foodEmg" type="checkbox" tabindex="550"></td>
              <td><textarea id="foodDesc" name="foodDesc" tabindex="560" class="narrowTextarea"></textarea></td>
            </tr>
            <tr>
              <td>Employment</td>
              <td align="center"><input name="employmentService" type="checkbox" tabindex="570"></td>
              <td align="center"><input name="employmentSupply" type="checkbox" tabindex="580"></td>
              <td align="center"><input name="employmentEmg" type="checkbox" tabindex="590"></td>
              <td><textarea id="employmentDesc" name="employmentDesc" tabindex="600" class="narrowTextarea"></textarea></td>
            </tr>
            <tr>
              <td>Mentoring</td>
              <td align="center"><input name="mentoringService" type="checkbox" tabindex="610"></td>
              <td align="center"><input name="mentoringSupply" type="checkbox" tabindex="620"></td>
              <td align="center"><input name="mentoringEmg" type="checkbox" tabindex="630"></td>
              <td><textarea id="mentoringDesc" name="mentoringDesc" tabindex="640" class="narrowTextarea"></textarea></td>
            </tr>
            <tr>
              <td>Counseling/Therapy</td>
              <td align="center"><input name="counselingService" type="checkbox" tabindex="650"></td>
              <td align="center"><input name="counselingSupply" type="checkbox" tabindex="660"></td>
              <td align="center"><input name="counselingEmg" type="checkbox" tabindex="670"></td>
              <td><textarea id="counselingDesc" name="counselingDesc" tabindex="680" class="narrowTextarea"></textarea></td>
            </tr>
            <tr>
              <td>Pregnancy</td>
              <td align="center"><input name="pregnancyService" type="checkbox" tabindex="690"></td>
              <td align="center"><input name="pregnancySupply" type="checkbox" tabindex="700"></td>
              <td align="center"><input name="pregnancyEmg" type="checkbox" tabindex="710"></td>
              <td><textarea id="pregnancDesc" name="pregnancyDesc" tabindex="720" class="narrowTextarea"></textarea></td>
            </tr>
            <tr>
              <td>Medical</td>
              <td align="center"><input name="medicalService" type="checkbox" tabindex="730"></td>
              <td align="center"><input name="medicalSupply" type="checkbox" tabindex="740"></td>
              <td align="center"><input name="medicalEmg" type="checkbox" tabindex="750"></td>
              <td><textarea id="medicalDesc" name="medicalDesc" tabindex="760" class="narrowTextarea"></textarea></td>
            </tr>
            <tr>
              <td>Legal</td>
              <td align="center"><input name="legalService" type="checkbox" tabindex="770"></td>
              <td align="center"><input name="legalSupply" type="checkbox" tabindex="780"></td>
              <td align="center"><input name="legalEmg" type="checkbox" tabindex="790"></td>
              <td><textarea id="legalDesc" name="legalDesc" tabindex="800" class="narrowTextarea"></textarea></td>
            </tr>
             <tr>
              <td>Governmental</td>
              <td align="center"><input name="governmentService" type="checkbox" tabindex="810"></td>
              <td align="center"><input name="governmentSupply" type="checkbox" tabindex="820"></td>
              <td align="center"><input name="governmentEmg" type="checkbox" tabindex="830"></td>
              <td><textarea id="governmentDesc" name="governmentDesc" tabindex="840" class="narrowTextarea"></textarea></td>
            </tr>
            <tr>
              <td>Investigation</td>
              <td align="center"><input name="investService" type="checkbox" tabindex="850"></td>
              <td align="center"><input name="investSupply" type="checkbox" tabindex="860"></td>
              <td align="center"><input name="investEmg" type="checkbox" tabindex="870"></td>
              <td><textarea id="investDesc" name="investDesc" tabindex="880" class="narrowTextarea"></textarea></td>
            </tr>
             <tr>
              <td>Foster Care</td>
              <td align="center"><input name="fosterService" type="checkbox" tabindex="890"></td>
              <td align="center"><input name="fosterSupply" type="checkbox" tabindex="900"></td>
              <td align="center"><input name="fosterEmg" type="checkbox" tabindex="910"></td>
              <td><textarea id="fosterDesc" name="fosterDesc" tabindex="920" class="narrowTextarea"></textarea></td>
            </tr>
            <tr>
              <td>Awareness/Education</td>
              <td align="center"><input name="awarenessService" type="checkbox" tabindex="930"></td>
              <td align="center"><input name="awarenessSupply" type="checkbox" tabindex="940"></td>
              <td align="center"><input name="awarenessEmg" type="checkbox" tabindex="950"></td>
              <td><textarea id="awarenessDesc" type="text" name="awarenessDesc" tabindex="960" class="narrowTextarea"></textarea></td>
            </tr>
            <tr>
              <td>Response Training</td>
              <td align="center"><input name="responseService" type="checkbox" tabindex="970"></td>
              <td align="center"><input name="responseSupply" type="checkbox" tabindex="980"></td>
              <td align="center"><input name="responseEmg" type="checkbox" tabindex="990"></td>
              <td><textarea id="responseDesc" name="responseDesc" tabindex="1000" class="narrowTextarea"></textarea></td>
            </tr>
            <tr>
              <td>Other</td>
              <td align="center"><input name="otherService" type="checkbox" tabindex="1010"></td>
              <td align="center"><input name="otherSupply" type="checkbox" tabindex="1020"></td>
              <td align="center"><input name="otherEmg" type="checkbox" tabindex="1030"></td>
              <td><textarea id="otherDesc" name="otherDesc" tabindex="104" class="narrowTextarea"></textarea></td>
            </tr>
          </tbody>
        </table>
       <table width="720" border="0" cellpadding="8" cellspacing="0">
           <tbody>
               <tr>
                   <td class="heads darkgray" colspan="2">Additional Considerations</td>
               </tr>
               <tr class="gray">
                   <td class="width-150"><input type="checkbox" id="chkFree" name="chkFree" onChange="feeChanged(this)" tabindex="1050"> Free 
                   <br/>
                   <input type="checkbox" id="chkFee" name="chkFee" onChange="feeChanged(this)" tabindex="1060"/> Associated Fee</td>
                   <td class="align-bottom"><textarea class="wideTextarea" id="fee" name="fee" onChange="feeChanged(this)" tabindex="1070"></textarea></td>    
               </tr>
               <tr class="gray">
                   <td class="width-150"><input type="checkbox" name="chkFaith" tabindex="1080"> Faith Based</td>
                   <td bgcolor="#F3F0F0"><textarea class="wideTextarea" name="faith" tabindex="1090"></textarea></td>
               </tr>
               <tr class="darkpink">
                   <td class="heads" colspan="2">Requirements</td>
               </tr>
               <tr class="pink">
                   <td class="width-150"><input type="checkbox" id="chkMembership" readonly tabindex="1100"> Membership</td>
                   <td bgcolor="#FBEAEB"><textarea id='membership' name="membership" class="wideTextarea" tabindex="1110"></textarea></td>
               </tr>
               <tr class="pink">
                   <td class="width-150"><input type="checkbox" id="chkTraining" readonly tabindex="1120"> Training</td>
                   <td bgcolor="#FBEAEB"><textarea class="wideTextarea" id="training" name="training" tabindex="1130"></textarea></td>
               </tr>
               <tr class="pink">
                   <td class="width-150"><input type="checkbox" id="chkApplication" readonly tabindex="1140"> Application</td>
                   <td bgcolor="#FBEAEB"><textarea class="wideTextarea" id="application" name="application" tabindex="1150"></textarea></td>
               </tr>
               <tr class="pink">
                   <td class="width-150"><input type="checkbox" id="chkWaiting" readonly tabindex="1160"> Waiting</td>
                   <td bgcolor="#FBEAEB"><textarea class="wideTextarea" id="waiting" name="waiting" tabindex="1170"></textarea></td>
               </tr>
           </tbody>
       </table>
      
       <table width="720" border="0" cellpadding="8" cellspacing="0">
            <tbody>
                <tr>
                    <td class="heads darkgray" colspan="2">Gender (Demographics)</td>
                </tr>
                <tr class="gray">
                    <td><input id="male" name="male" type="checkbox" tabindex="1180"><label for="male"> Male</label></td>
                    <td><input id="female" name="female" type="checkbox" tabindex="1190"><label for="female"> Female</label></td>
                </tr>
                <tr class="gray">
                   <td><input id="transMale" name="transMale" type="checkbox" tabindex="1200"><label for="transMale"> Transgender Male</label></td>
                   <td><input id="transFemale" name="transFemale" type="checkbox" tabindex="1210"><label for="transFemale"> Transgender Female</label></td>
                </tr>
                <tr>
                    <td class="heads darkgray" colspan="2">Age (Demographics)</td>
                </tr>
                <tr class="gray">
                    <td><input id="infant" name="infant" type="checkbox" tabindex="1220"><label for="infant"> Infant/Toddler</label></td>
                    <td><input id="children" name="children" type="checkbox" tabindex="1230"><label for="children"> Children</label></td>
                </tr>
                <tr class="gray">
                    <td><input id="youth" name="youth" type="checkbox" tabindex="1240"><label for="youth"> Youth/Young Adults</label></td>
                    <td><input id="adults" name="adults" type="checkbox" tabindex="1250"><label for="adults"> Adults</label></td>
                </tr>
            </tbody>
        </table>
        <table width="720" border="0" cellpadding="8" cellspacing="0">
            <tbody>
                <tr class="gray">
                    <td class="width-150"><label for="ageRanges">Age Ranges </label></td>
                    <td><textarea id="ageRanges" name="ageRanges" class="wideTextarea" tabindex="1260"></textarea></td>
                </tr>
                <tr>
                    <td class="heads darkgray" colspan="2">Nationality (Demographics)</td>
                </tr>
                <tr class="gray">
                    <td><input id="domestic" name="domestic" type="checkbox" tabindex="1270"> <label for="domestic"> Domestic-Born</label></td>
                    <td><input id="foreign" name="foreign" type="checkbox" tabindex="1280"> <label for="foreign"> Foreign-Born</label></td>
                </tr>
                <tr class="gray">
                    <td><input id="undocumented" name="undocumented" type="checkbox" tabindex="1290"> <label for="undocumented">Undocumented</label></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
        <table width="720" border="0" cellpadding="8" cellspacing="0">
            <tbody>
                <tr class="gray">
                    <td class="width-150"><label for="race">Race </label></td>
                    <td><textarea id="race" name="race" class="wideTextarea" tabindex="1300"></textarea></td>
                </tr>
                <tr class="gray">
                    <td class="width-150"><label for="ethnicity">Ethnicity </label></td>
                    <td><textarea id="ethnicity" name="ethnicity" class="wideTextarea" tabindex="1310"></textarea></td>
                </tr>
                <tr class="darkpink">
                    <td colspan="2">Notes</td>
                </tr>
                <tr class="pink">
                  <td colspan="2"><textarea id="notes" name="notes" cols="75" rows="5" maxlength="500" tabindex="1320"></textarea></td>
                </tr>
                <tr class="darkblue">
                    <td class="heads" colspan="2">Confidential Notes</td>
                </tr>
                <tr class="blue">
                  <td colspan="2"><textarea name="conNotes" cols="75" rows="5" maxlength="500" id="conNotes" tabindex="1330"></textarea></td>
                </tr>
                <tr class="darkblue">
                    <td class="heads" colspan="2">Office Use Only</td>
                </tr>
                <tr class="blue">
                    <td><input type="checkbox" name="warReview" id="warReview" tabindex="1340" ><label for="warReview">WAR Review</label></td>
                    <td style="color:gray;">Program ID (pgm_id):<label id="pgm_id" ></label></td>
                </tr>
                <tr>
                    <td></td>
                    <td align="right"><input class="buttonSubmit" name="Reset" value="Reset" onClick="resetForm()" type="button">&nbsp;&nbsp;<input class="buttonSubmit" name="submit" onClick="addRecord()" value="Submit" type="button" tabindex="1350"></td>
                </tr>
            </tbody>
        </table>
    </form>
       
<script>
        togglePrimeContactConfidential();  
        feeChanged();
</script>
//is program confidential checkbox
	if(document.getElementById("confidential").checked==true){
		recordArray[0]=true;
	}else{
		recordArray[0]=false;
	}
	//Program Main Info
	recordArray[1]=document.getElementById("programName").value;
	recordArray[2]=document.getElementById("umbrella").value;
	recordArray[3]=document.getElementById("statement").value;
	recordArray[4]=document.getElementById("phone").value;
	recordArray[5]=document.getElementById("hotline").value;
	recordArray[6]=document.getElementById("website").value;
	recordArray[7]=document.getElementById("email").value;
	//Confidential Phone Number
	recordArray[8]=document.getElementById("conPhone").value;
	if(document.getElementById("contactConfidential").checked==true){
		recordArray[9]=true;
	}else{
		recordArray[9]=false;
	}
	//Primary Contact
	recordArray[10]=document.getElementById("priName").value;
	recordArray[11]=document.getElementById("priPosition").value;
	recordArray[12]=document.getElementById("priPhone").value;
	recordArray[13]=document.getElementById("priEmail").value;
	//Program Address
	recordArray[14]=document.getElementById("street").value;
	recordArray[15]=document.getElementById("city").value;
	recordArray[16]=document.getElementById("county").value;
	recordArray[17]=document.getElementById("state").value;
	recordArray[18]=document.getElementById("zip").value;
	//Program Address 2
	recordArray[19]=document.getElementById("street2").value;
	recordArray[20]=document.getElementById("city2").value;
	recordArray[21]=document.getElementById("county2").value;
	recordArray[22]=document.getElementById("state2").value;
	recordArray[23]=document.getElementById("zip2").value;
	//Confidential Address
	recordArray[24]=document.getElementById("street3").value;
	recordArray[25]=document.getElementById("city3").value;
	recordArray[26]=document.getElementById("county3").value;
	recordArray[27]=document.getElementById("state3").value;
	recordArray[28]=document.getElementById("zip3").value;
	//Hours
	//24/7 check box
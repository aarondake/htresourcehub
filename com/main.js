var resource = false;
var age = false;
var gender = false;
var national = false;
var blocation = false;
var isLocked = true;
var role = "guest";
var filter="";
var recordArray=[];
var resources=[];
var demographics=[];

function search() {
    filter = "resources=";
    crumbs="";
    // loop through resources parameters
    for (i = 0; i < resources.length; i++) { 
        if(resources[i]!== undefined) {
            filter = filter.concat(resources[i],',');
            crumbs = crumbs+"<span style='color:#666666;'> | </span>"+resources[i];
        }
    }
    // if there are no resources, then remove the resources parameter
    // in any case, add the demographics parameter :)
    if (filter === "resources=") {
        // only one paramter now, so just add this one, no need for '&' before.
        filter="demographics=";
    }else{
        // removes the last comma ","
        filter = filter.substr(0,filter.length - 1);
        // tacks on the next parameters: demographics.
        filter = filter.concat("&demographics=");
    }
    
    // loop through demographics parameters
    // Start with 16 because we start the array at that, due to Dave's previous coding.  -ALD
    for (i = 16; i < demographics.length; i++) { 
        if(demographics[i]!== undefined) {
            filter = filter.concat(demographics[i],',');
            crumbs = crumbs+"<span style='color:#666666;'> | </span>"+demographics[i];
        }
    }
    // removes the last comma ","
    filter = filter.substr(0,filter.length - 1);

	if(resources.length==0) {
		breadcrumbs.innerHTML = "<h4>Welcome to Human Trafficking Resource Hub</h4>";
	}else{
		if(crumbs=="") {
			breadcrumbs.innerHTML = "<h4>Welcome to Human Trafficking Resource Hub</h4>";
		}else{
			breadcrumbs.innerHTML = "<span style='color:#666666;'> Selected: </span>"+crumbs;
		}
	}
	loadXMLDoc("search.php", filter, showResults);
}
function resetMenus() {
	alert("reset menu");
}
function setResults(HTML) {
    document.getElementById("results").innerHTML=HTML;
}

/*function loadXMLDoc()
{
    var postRequest;
    if (window.XMLHttpRequest)
      {// code for IE7+, Firefox, Chrome, Opera, Safari
      postRequest=new XMLHttpRequest();
      }
    else
      {// code for IE6, IE5
      postRequest=new ActiveXObject("Microsoft.XMLHTTP");
      }
    postRequest.onreadystatechange=function()
      {
      if (postRequest.readyState==4 && postRequest.status==200)
        {
            setResults(postRequest.responseText);
        }
      };

    // for (var i=1; i<filter.length; i++ ) {   }
    postRequest.open("GET","search.php".concat(filter),true);
    postRequest.send();
}*/

var postRequest;
function loadXMLDoc(url,parameters, cfunc) {
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        postRequest=new XMLHttpRequest();
    }else{
        // code for IE6, IE5
        postRequest=new ActiveXObject("Microsoft.XMLHTTP");
    }

    postRequest.onreadystatechange=function(){
    if (postRequest.readyState===4){
      if (postRequest.status===200 || window.location.href.indexOf("http")===-1){
          cfunc();
      }
      else{
       alert("An error has occured making the request");
      }
     }
    };
    //postRequest.onreadystatechange=cfunc;
    /*postRequest.open("GET",url,true);
    postRequest.send();*/
    postRequest.open("POST",url,true);
    postRequest.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    postRequest.send(parameters);
}

function showResults() { 
    if (postRequest.readyState===4 && postRequest.status===200) {
        document.getElementById("results").innerHTML=postRequest.responseText;
    }
}

function showDetails(id) {
    alert(id);
    loadXMLDoc("searchDetails.php?id="+id,function(){
        if (postRequest.readyState===4 && postRequest.status===200) {
            document.getElementById("light").innerHTML=postRequest.responseText;
            ligtbox_open();
        }
    });
}

function resetForm() {
   //if (confirm('Reset all field values?')) {
        document.getElementById("WARchest").reset();
    //}
}
function addRecord() {
    var str = "";
    var elements = document.getElementById('WARchest').elements;
    for (var i = 0; i< elements.length; i++) {
        if (str.length > 1) {str += "&";}
        if (elements[i].type === "checkbox"){
            if (elements[i].checked) {
                elements[i].value = 1;
            }else {
                elements[i].value = 0;
            }
        }
        str += elements[i].name + "=" + encodeURIComponent(elements[i].value);
    }   
    
    loadXMLDoc("addRecord.php", str, recordAdded);
}
function recordAdded() {
    var msg = "";
    if (postRequest.responseText.length > 0) {
        // THERE WAS AN ERROR!  OOPS...
        msg = postRequest.responseText;
    } else {
        // RECORD ENTERED SUCCESSFULLY!
        msg = "Record Added Successfully!";
        resetForm(); //clears the enterd values of the form
    }
    alert(msg);
    document.getElementById("form-messages").innerHTML = msg;    
}

function checkUser() {
	if(calc(document.getElementById('loginform').thePass.value)=="fbc5bc5fd5ee1fb52fb055b6e5bca673"){
		TweenLite.to(login, 0.5, {css:{visibility:"hidden"}});
		TweenLite.to(logout, 0.5, {css:{visibility:"visible"}});
		TweenLite.to(menuRecords, 0.5, {css:{visibility:"visible"}});
		TweenLite.to(menuLogin, 1, {css:{top:"-65px"},ease:Quad.easeOut});
		role="war";
		isLocked=false;
		document.getElementById('loginform').thePass.value = "";
                
	}else if(calc(document.getElementById('loginform').thePass.value)=="4acb4bc224acbbe3c2bfdcaa39a4324e"){
		TweenLite.to(login, 0.5, {css:{visibility:"hidden"}});
		TweenLite.to(logout, 0.5, {css:{visibility:"visible"}});
		TweenLite.to(menuLogin, 1, {css:{top:"-65px"},ease:Quad.easeOut});
		role="admin";
		isLocked=false;
		document.getElementById('loginform').thePass.value = "";
	}else if(calc(document.getElementById('loginform').thePass.value)=="504c314447e32ded4096fb0b470ace4f"){
		TweenLite.to(login, 0.5, {css:{visibility:"hidden"}});
		TweenLite.to(logout, 0.5, {css:{visibility:"visible"}});
		TweenLite.to(menuLogin, 1, {css:{top:"-65px"},ease:Quad.easeOut});
		role="editor";
		isLocked=false;
		document.getElementById('loginform').thePass.value = "";
	}else{
		alert("sorry password does not match");
		TweenLite.to(menuLogin, 1, {css:{top:"-65px"},ease:Quad.easeOut});
		TweenLite.to(openit, 0.5, {css:{visibility:"hidden"}});
		TweenLite.to(menuRecords, 0.5, {css:{visibility:"hidden"}});
		TweenLite.to(menuResource, 0.5, {css:{visibility:"hidden"}});
		TweenLite.to(newRecord, 0.5, {css:{visibility:"hidden"}});
		document.getElementById('loginform').thePass.value = "";
		role="guest";
	}	
}
function enterInfo() {
	menuReset();
	TweenLite.to(results, 0.5, {css:{visibility:"hidden"}});
	TweenLite.to(newRecord, 0.5, {css:{visibility:"visible"}});
}
function theUser() {
	if(isLocked===true) {
		TweenLite.to(menuLogin, 1, {css:{top:"65px"},ease:Quad.easeOut});
                document.getElementById("loginform").thePass.focus();
	} else {
		TweenLite.to(results, 0.5, {css:{visibility:"visible"}});
		TweenLite.to(logout, 0.5, {css:{visibility:"hidden"}});
		TweenLite.to(login, 0.5, {css:{visibility:"visible"}});
		TweenLite.to(newRecord, 0.5, {css:{visibility:"hidden"}});
		TweenLite.to(menuLogin, 1, {css:{top:"-65px"},ease:Quad.easeOut});
		isLocked=true;
	}
}
function aboutWrite() {
	var message = "<p>Human trafficking is a form of modern-day slavery in which people are treated as property, as possessions to be used, controlled, and exploited for the gain of others. Lured by deceit or forced by coercion, victims of trafficking are exploited for commercial, labor, or sexual purposes. Though traffickers typically prey on the most vulnerable, this crime knows no socioeconomic bounds—it takes place in communities rich and poor, urban and suburban, and is not a respecter of ethnicity, race, or economic status. Millions of people are trafficked in countries all over the world—in third-world countries and wealthy nations, including the United States, the &quot;land of the free.&quot;</p><p>If you are a victim of trafficking or any other form of abuse or exploitation, this site is here to help you.If you know someone who is a victim, or if you suspect someone you know may be, this site functions as a resource for you as well.Whether you need food and shelter, counseling, medical care, legal advice, or other assistance; whether you suspect a case of trafficking and need to know what signs to look for; whether you want to know how you can help; or whether you are simply seeking information, this site exists to provide the resources and information you need.</p>"


	document.getElementById("results").innerHTML = message;	
}
window.document.onkeydown = function (e)
{
    if (!e){
        e = event;
    }
    if (e.keyCode === 27){
        lightbox_close();
    }
};
function lightbox_open(){
    window.scrollTo(0,0);
    document.getElementById('light').style.display='block';
    document.getElementById('fade').style.display='block';  
}
function lightbox_close(){
    document.getElementById('light').style.display='none';
    document.getElementById('fade').style.display='none';
}
function checkShelter() {
	if(resources[0]==undefined || resources[0]=="") {
   		 TweenLite.to(check1, 2, {css:{background:"#ae0001"}});
        resources[0]="Housing-Shelter";
	}else{
		TweenLite.to(check1, 2, {css:{background:"#ffffff"}});
		resources[0]=undefined;
	}
	search();
}
function checkTransit() {
	if(resources[1]==undefined || resources[1]=="") {
   		 TweenLite.to(check2, 2, {css:{background:"#ae0001"}});
        resources[1]="Housing-Transitional";
	}else{
		TweenLite.to(check2, 2, {css:{background:"#ffffff"}});
		resources[1]=undefined;
	}
	search();
}
function checkLocate() {
	if(resources[2]==undefined || resources[2]=="") {
   		 TweenLite.to(check3, 2, {css:{background:"#ae0001"}});
        resources[2]="Housing-Locate";
	}else{
		TweenLite.to(check3, 2, {css:{background:"#ffffff"}});
		resources[2]=undefined;
	}
	search();
}
function checkClothing() {	
	if(resources[3]==undefined || resources[3]=="") {
   		 TweenLite.to(check4, 2, {css:{background:"#ae0001"}});
        resources[3]="Clothing";
	}else{
		TweenLite.to(check4, 2, {css:{background:"#ffffff"}});
		resources[3]=undefined;
	}
	search();
}
function checkFood() {	
	if(resources[4]==undefined || resources[4]=="") {
   		 TweenLite.to(check5, 2, {css:{background:"#ae0001"}});
        resources[4]="Food";
	}else{
		TweenLite.to(check5, 2, {css:{background:"#ffffff"}});
		resources[4]=undefined;
	}
	search();
}
function checkEmployment() {	
	if(resources[5]==undefined || resources[5]=="") {
   		 TweenLite.to(check6, 2, {css:{background:"#ae0001"}});
        resources[5]="Employment";
	}else{
		TweenLite.to(check6, 2, {css:{background:"#ffffff"}});
		resources[5]=undefined;
	}
	search();
}
function checkMentoring() {	
	if(resources[6]==undefined || resources[6]=="") {
   		 TweenLite.to(check7, 2, {css:{background:"#ae0001"}});
        resources[6]="Mentoring";
	}else{
		TweenLite.to(check7, 2, {css:{background:"#ffffff"}});
		resources[6]=undefined;
	}
	search();
}
function checkCounseling() {	
	if(resources[7]==undefined || resources[7]=="") {
   		 TweenLite.to(check8, 2, {css:{background:"#ae0001"}});
        resources[7]="Counseling";
	}else{
		TweenLite.to(check8, 2, {css:{background:"#ffffff"}});
		resources[7]=undefined;
	}
	search();
}
function checkPregnancy() {	
	if(resources[8]==undefined || resources[8]=="") {
   		 TweenLite.to(check9, 2, {css:{background:"#ae0001"}});
        resources[8]="Pregnancy";
	}else{
		TweenLite.to(check9, 2, {css:{background:"#ffffff"}});
		resources[8]=undefined;
	}
	search();
}
function checkMedical() {	
	if(resources[9]==undefined || resources[9]=="") {
   		 TweenLite.to(check10, 2, {css:{background:"#ae0001"}});
        resources[9]="Medical";
	}else{
		TweenLite.to(check10, 2, {css:{background:"#ffffff"}});
		resources[9]=undefined;
	}
	search();
}
function checkLegal() {	
	if(resources[10]==undefined || resources[10]=="") {
   		 TweenLite.to(check11, 2, {css:{background:"#ae0001"}});
        resources[10]="Legal";
	}else{
		TweenLite.to(check11, 2, {css:{background:"#ffffff"}});
		resources[10]=undefined;
	}
	search();
}
function checkGovernmental() {	
	if(resources[11]==undefined || resources[11]=="") {
   		 TweenLite.to(check12, 2, {css:{background:"#ae0001"}});
        resources[11]="Government";
	}else{
		TweenLite.to(check12, 2, {css:{background:"#ffffff"}});
		resources[11]=undefined;
	}
	search();
}
function checkInvest() {	
	if(resources[12]==undefined || resources[12]=="") {
   		 TweenLite.to(check13, 2, {css:{background:"#ae0001"}});
        resources[12]="Investgation";
	}else{
		TweenLite.to(check13, 2, {css:{background:"#ffffff"}});
		resources[12]=undefined;
	}
	search();
}
function checkFoster() {	
	if(resources[13]==undefined || resources[13]=="") {
   		 TweenLite.to(check14, 2, {css:{background:"#ae0001"}});
        resources[13]="Foster-Care";
	}else{
		TweenLite.to(check14, 2, {css:{background:"#ffffff"}});
		resources[13]=undefined;
	}
	search();
}
function checkAware() {	
	if(resources[14]==undefined || resources[14]=="") {
   		 TweenLite.to(check15, 2, {css:{background:"#ae0001"}});
        resources[14]="Awareness-Education";
	}else{
		TweenLite.to(check15, 2, {css:{background:"#ffffff"}});
		resources[14]=undefined;
	}
	search();
}
function checkResponse() {	
	if(resources[15]==undefined || resources[15]=="") {
   		 TweenLite.to(check16, 2, {css:{background:"#ae0001"}});
        resources[15]="Response-Training";
	}else{
		TweenLite.to(check16, 2, {css:{background:"#ffffff"}});
		resources[15]=undefined;
	}
	search();
}
function checkToddler() {	
	if(demographics[16]==undefined || demographics[16]=="") {
   		 TweenLite.to(check17, 2, {css:{background:"#ae0001"}});
        demographics[16]="Infant-Toddler";
	}else{
		TweenLite.to(check17, 2, {css:{background:"#ffffff"}});
		demographics[16]=undefined;
	}
       search();	
}
function checkChildren() {	
	if(demographics[17]==undefined || demographics[17]=="") {
   		 TweenLite.to(check18, 2, {css:{background:"#ae0001"}});
        demographics[17]="Children";
	}else{
		TweenLite.to(check18, 2, {css:{background:"#ffffff"}});
		demographics[17]=undefined;
	}
       search();	
}
function checkYouth() {	
	if(demographics[18]==undefined || demographics[18]=="") {
   		 TweenLite.to(check19, 2, {css:{background:"#ae0001"}});
        demographics[18]="Youth";
	}else{
		TweenLite.to(check19, 2, {css:{background:"#ffffff"}});
		demographics[18]=undefined;
	}
      search();	
}
function checkAdult() {	
	if(demographics[19]==undefined || demographics[19]=="") {
   		 TweenLite.to(check20, 2, {css:{background:"#ae0001"}});
        demographics[19]="Adult";
	}else{
		TweenLite.to(check20, 2, {css:{background:"#ffffff"}});
		demographics[19]=undefined;
	}
      search();	
}
function checkMale() {	
	if(demographics[20]==undefined || demographics[20]=="") {
   		 TweenLite.to(check21, 2, {css:{background:"#ae0001"}});
        demographics[20]="Male";
	}else{
		TweenLite.to(check21, 2, {css:{background:"#ffffff"}});
		demographics[20]=undefined;
	}
       search();	
}
function checkFemale() {	
	if(demographics[21]==undefined || demographics[21]=="") {
   		 TweenLite.to(check22, 2, {css:{background:"#ae0001"}});
        demographics[21]="Female";
	}else{
		TweenLite.to(check22, 2, {css:{background:"#ffffff"}});
		demographics[21]=undefined;
	}
       search();	
}
function checkTransMale() {	
	if(demographics[22]==undefined || demographics[22]=="") {
   		 TweenLite.to(check23, 2, {css:{background:"#ae0001"}});
        demographics[22]="Transgender-Male";
	}else{
		TweenLite.to(check23, 2, {css:{background:"#ffffff"}});
		demographics[22]=undefined;
	}
       search();	
}
function checkTransFemale() {	
	if(demographics[23]==undefined || demographics[23]=="") {
   		 TweenLite.to(check24, 2, {css:{background:"#ae0001"}});
        demographics[23]="Transgender-Female";
	}else{
		TweenLite.to(check24, 2, {css:{background:"#ffffff"}});
		demographics[23]=undefined;
	}
      search();	
}
function checkDomestic() {	
	if(demographics[24]==undefined || demographics[24]=="") {
   		 TweenLite.to(check25, 2, {css:{background:"#ae0001"}});
        demographics[24]="Domestic-Born";
	}else{
		TweenLite.to(check25, 2, {css:{background:"#ffffff"}});
		demographics[24]=undefined;
	}
       search();	
}
function checkForeign() {	
	if(demographics[25]==undefined || demographics[25]=="") {
   		 TweenLite.to(check26, 2, {css:{background:"#ae0001"}});
        demographics[25]="Foreign-Born";
	}else{
		TweenLite.to(check26, 2, {css:{background:"#ffffff"}});
		demographics[25]=undefined;
	}
       search();	
}
function checkUndoc() {	
	if(demographics[26]==undefined || demographics[26]=="") {
   		 TweenLite.to(check27, 2, {css:{background:"#ae0001"}});
        demographics[26]="Undocumented";
	}else{
		TweenLite.to(check27, 2, {css:{background:"#ffffff"}});
		demographics[26]=undefined;
	}
       search();	
}
function checkState() {	
	  resources[27]=document.getElementById('states').selectState.value;
	  if(document.getElementById('states').selectState.value=="Michigan") {
		 TweenLite.to(counties, 0, {css:{visibility:"visible"}});
	  } else {
		  TweenLite.to(counties, 0, {css:{visibility:"hidden"}});
	  }
      search();	
}
function checkCounty() {
	  resources[28]=document.getElementById('counties').County.value;
      search();	
}
function menuReset() {
	TweenLite.to(menuResource, 0.5, {css:{left:"300px"}});
	TweenLite.to(menuResource, 0.5, {css:{opacity:0}});
	TweenLite.to(plusResource, 0.5, {css:{opacity:100}});
	resource=false;
	TweenLite.to(menuAge, 0.5, {css:{left:"300px"}});
	TweenLite.to(menuAge, 0.5, {css:{opacity:0}});
	TweenLite.to(plusAge, 0.5, {css:{opacity:100}});
	age=false;
	TweenLite.to(menuGender, 0.5, {css:{left:"300px"}});
	TweenLite.to(menuGender, 0.5, {css:{opacity:0}});
	TweenLite.to(plusGender, 0.5, {css:{opacity:100}});
	gender=false;
	TweenLite.to(menuNational, 0.5, {css:{left:"300px"}});
	TweenLite.to(menuNational, 0.5, {css:{opacity:0}});
	TweenLite.to(plusNational, 0.5, {css:{opacity:100}});
	national=false;
	TweenLite.to(menuLocation, 0.5, {css:{left:"300px"}});
	TweenLite.to(menuLocation, 0.5, {css:{opacity:0}});
	TweenLite.to(plusLocation, 0.5, {css:{opacity:100}});
	blocation=false;
	//make login disappear if displayed.
	TweenLite.to(menuLogin, 1, {css:{top:"-65px"},ease:Quad.easeOut});
}
function goResource() {
	if(resource==false) {
		menuReset();
		TweenLite.to(menuResource, 1, {css:{left:"15px"},ease:Quad.easeOut});
		TweenLite.to(menuResource, 0.2, {css:{opacity:100}});
		TweenLite.to(plusResource, 0.5, {css:{opacity:0}});
		TweenLite.to(results, 0.5, {css:{visibility:"visible"}});
		TweenLite.to(newRecord, 0.5, {css:{visibility:"hidden"}});
                resource=true;
	}else{
		TweenLite.to(menuResource, 0.5, {css:{left:"300px"}});
		TweenLite.to(menuResource, .2, {css:{opacity:0}});
		TweenLite.to(plusResource, 0.5, {css:{opacity:100}});
		TweenLite.to(results, 0.5, {css:{visibility:"visible"}});
		TweenLite.to(newRecord, 0.5, {css:{visibility:"hidden"}});
		resource=false;
	}
}
//buttonResource.goResource();
function goAge() {
	if(age==false) {
		menuReset();
		TweenLite.to(menuAge, 1, {css:{left:"15px"},ease:Quad.easeOut});
		TweenLite.to(menuAge, 0.2, {css:{opacity:100}});
		TweenLite.to(plusAge, 0.5, {css:{opacity:0}});
		TweenLite.to(results, 0.5, {css:{visibility:"visible"}});
		TweenLite.to(newRecord, 0.5, {css:{visibility:"hidden"}});
		age=true;
	}else{
		TweenLite.to(menuAge, 0.5, {css:{left:"300px"}});
		TweenLite.to(menuAge, 0.2, {css:{opacity:0}});
		TweenLite.to(plusAge, 0.5, {css:{opacity:100}});
		TweenLite.to(results, 0.5, {css:{visibility:"visible"}});
		TweenLite.to(newRecord, 0.5, {css:{visibility:"hidden"}});
		age=false;
	}
}
//buttonAge.goAge();
function goGender() {
	if(gender==false) {
		menuReset();
		TweenLite.to(menuGender, 1, {css:{left:"15px"},ease:Quad.easeOut});
		TweenLite.to(menuGender, 0.2, {css:{opacity:100}});
		TweenLite.to(plusGender, 0.5, {css:{opacity:0}});
		TweenLite.to(results, 0.5, {css:{visibility:"visible"}});
		TweenLite.to(newRecord, 0.5, {css:{visibility:"hidden"}});
		gender=true;
	}else{
		TweenLite.to(menuGender, 0.5, {css:{left:"300px"}});
		TweenLite.to(menuGender, 0.2, {css:{opacity:0}});
		TweenLite.to(plusGender, 0.5, {css:{opacity:100}});
		TweenLite.to(results, 0.5, {css:{visibility:"visible"}});
		TweenLite.to(newRecord, 0.5, {css:{visibility:"hidden"}});
		gender=false;
	}
}
//buttonGender.goGender();
function goNational() {
	if(national==false) {
		menuReset();
		TweenLite.to(menuNational, 1, {css:{left:"15px"},ease:Quad.easeOut});
		TweenLite.to(menuNational, 0.2, {css:{opacity:100}});
		TweenLite.to(plusNational, 0.5, {css:{opacity:0}});
		TweenLite.to(results, 0.5, {css:{visibility:"visible"}});
		TweenLite.to(newRecord, 0.5, {css:{visibility:"hidden"}});
		national=true;
	}else{
		TweenLite.to(menuNational, 0.5, {css:{left:"300px"}});
		TweenLite.to(menuNational, 0.2, {css:{opacity:0}});
		TweenLite.to(plusNational, 0.5, {css:{opacity:100}});
		TweenLite.to(results, 0.5, {css:{visibility:"visible"}});
		TweenLite.to(newRecord, 0.5, {css:{visibility:"hidden"}});
		national=false;
	}
}
function goLocation() {
	if(blocation==false) {
		menuReset();
		TweenLite.to(menuLocation, 1, {css:{left:"15px"},ease:Quad.easeOut});
		TweenLite.to(menuLocation, 0.2, {css:{opacity:100}});
		TweenLite.to(plusLocation, 0.5, {css:{opacity:0}});
		TweenLite.to(results, 0.5, {css:{visibility:"visible"}});
		TweenLite.to(newRecord, 0.5, {css:{visibility:"hidden"}});
		blocation=true;
	}else{
		TweenLite.to(menuLocation, 0.5, {css:{left:"300px"}});
		TweenLite.to(menuLocation, 0.2, {css:{opacity:0}});
		TweenLite.to(plusLocation, 0.5, {css:{opacity:100}});
		TweenLite.to(results, 0.5, {css:{visibility:"visible"}});
		TweenLite.to(newRecord, 0.5, {css:{visibility:"hidden"}});
		blocation=false;
	}
}
//buttonNational.goNational();